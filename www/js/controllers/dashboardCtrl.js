angular.module('starter.controllers')

.controller('DashboardCtrl', function($scope, $ionicPopup, $ionicLoading, UserService, NomesEventosService) {

  var showLoading = function(){
        $ionicLoading.show({
          template: 'Aguarde...'
        });
      };

  var hideLoading = function(){
       $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

/*
  $scope.$on(NomesEventosService.novaSolicitacao, function(event, athleteEmail){
    showAlert("Nova Solicitação", "Solicitação recebida do atleta: " + athleteEmail);
  });
*/

});