angular.module('starter.controllers')

.controller('LoginCtrl', function($scope, $ionicLoading, $ionicPopup, $state, AuthService, rx, AthleteService, Config, IntensidadeService, EstiloService, TimeEstimateService, UserService){

  $scope.loginData = {};
  $scope.status = "";

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  /*
    Verificar se o usuário já está logado

    Retorna true se o usuário já estiver logado
  */
  var verificaUsuarioLogado = function(){
    return UserService.verificaUsuarioLogado();
  }

  var getEmailUsuarioLogado = function(){
    return UserService.getEmailUsuarioLogado();
  }

  var getSenhaUsuarioLogado = function(){
    return UserService.getSenhaUsuarioLogado();
  }


  /**
    data.email e data.senha
  */
  $scope.doLogin = function(data){

      console.log("data", data);

      // Verificar se os campos login e senha foram preenchidos
      if(data.email && data.senha){
          showLoading();

          AuthService.autenticar(data.email, data.senha, function(response){

              console.log("Resposta da autenticação no controller");              

              if(response.status){

                  IntensidadeService.getAndRefreshIntensidadesListFromServer(function(erro){
                    hideLoading();
                    showAlert("Erro", "Erro ao atualizar lista de intensidades");

                  }, function(){

                    EstiloService.getAndRefreshEstilosListFromServer(function(erro){
                        hideLoading();
                        showAlert("Erro", "Erro ao atualizar lista de estilos");

                    }, function(){

                      TimeEstimateService.getAndRefreshIntervaloPorIntensidade(function(erro){
                        hideLoading();
                        showAlert("Erro", "Erro ao atualizar lista de intervalo por intensidade");

                      }, function(){

                          TimeEstimateService.getAndRefreshIntensidadeZonaTreinamentoFraco(function(erro){

                            hideLoading();
                            showAlert("Erro", "Erro ao atualizar lista de intensidade zona de treinamento fraco");

                          }, function(){

                              TimeEstimateService.getAndRefreshIntensidadeZonaTreinamentoForte(function(erro){

                                hideLoading();
                                showAlert("Erro", "Erro ao atualizar lista de intensidade zona de treinamento forte");

                              }, function(){

                                  hideLoading();
                                  var usuario = Config.getUsuario();
                                  UserService.setUsuarioLogado(usuario.email, usuario.senha);

                                  if(Config.isAthlete()){
                                      $state.go('app.athlete_treino_menu');

                                  }else{
                                      $state.go('app.coach_treino_menu');
                                  }

                              });

                          });

                      });

                    });

                  });

              }else{

                hideLoading();
                showAlert("Erro", response.msg);
              }

          });

      }
  };


  if(verificaUsuarioLogado()){

    console.log("Usuário já está logado no app");

    var data = {
      email : getEmailUsuarioLogado(),
      senha : getSenhaUsuarioLogado()
    };

    console.log("Verificar usuario já logado data: ", data);

    if(data.email && data.email.length > 0 && data.senha && data.senha.length > 0){
      $scope.doLogin(data);
    }
    
  }

});