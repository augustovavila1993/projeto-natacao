angular.module('starter.controllers')

.controller('TreinoCtrl', function($scope, TimeEstimateService, UserFriendlyService) {
  
  // getTempoMinASerFeito : function(sistemaEnergia, distancia, estilo){
  var min = TimeEstimateService.getTempoMinASerFeito('a1', 100, 'crawl');
  var max = TimeEstimateService.getTempoMaxASerFeito('a1', 100, 'crawl');
  console.log("min", min);
  console.log("max", max);

  // Buscar o treino selecionado
  var coachWorkout = {
      date : '2016-02-09',
      totalDistance : 2200,
      type : '',

      series : [{
        series : 1,  
        repetition : 4,
        distance : 100, // em metros
        style : 'crawl', // crawl, butterfly, backstroke, breaststroke
        intensity : 'a0' // sistemaEnergia: a0, a1, a2, a3, resan, tl pt, vl
      },{
        series : 2,  
        repetition : 8,
        distance : 50, // em metros
        style : 'crawl', // crawl, butterfly, backstroke, breaststroke
        intensity : 'resan' // sistemaEnergia: a0, a1, a2, a3, resan, tl pt, vl
      },{
        series : 1,  
        repetition : 10,
        distance : 100,//em metros
        style : 'crawl', // crawl, butterfly, backstroke, breaststroke
        intensity : 'a3'//sistemaEnergia: a0, a1, a2, a3, resan, tl pt, vl
      }]
  };

  var athleteWorkout = {
      date : coachWorkout.date,
      totalDistance : coachWorkout.totalDistance,
      type : coachWorkout.type,
      series : []
  };

  // Transformar o treino do técnico para o atleta
  // Calcular o tempo mínimo e máximo a ser feito para cada tiro do treino e intervalo
  coachWorkout.series.forEach(function(elem, index, arr){
    var athleteSerie = {};
    athleteSerie.series = elem.series;
    athleteSerie.repetition = elem.repetition;
    athleteSerie.distance = elem.distance;
    athleteSerie.style = UserFriendlyService.formatStyle( elem.style );
    athleteSerie.intensity = UserFriendlyService.formatSistemaEnergia( elem.intensity );
    athleteSerie.minTime = UserFriendlyService.fromSecondsToUserFriendlyTime(TimeEstimateService.getTempoMinASerFeito(elem.intensity, elem.distance, elem.style));
    athleteSerie.maxTime = UserFriendlyService.fromSecondsToUserFriendlyTime(TimeEstimateService.getTempoMaxASerFeito(elem.intensity, elem.distance, elem.style));

    // Calcular o intervalo
    var opcaoMetragem = 0;
    if(elem.distance <= 100){
      opcaoMetragem = 1;
    }else if(elem.distance > 100 && elem.distance < 400){
      opcaoMetragem = 2;
    }else{
      opcaoMetragem = 3;
    }
    athleteSerie.interval = UserFriendlyService.fromSecondsToUserFriendlyTime(TimeEstimateService.getInterval(elem.intensity, opcaoMetragem));
    athleteWorkout.series.push(athleteSerie);
  });

    $scope.athleteWorkout = athleteWorkout;
    console.log("athleteWorkout", athleteWorkout);
});