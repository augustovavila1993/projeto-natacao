angular.module('starter.controllers')

.controller('BestTimesCtrl', function($scope, $ionicLoading, $ionicPopup, AthleteService, UserFriendlyService) {

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  showLoading();

  AthleteService.getMelhoresTempos(function(){

    hideLoading();
    showAlert("Erro", "Verifique sua conexão com a internet e tente novamente mais tarde");

  }, function(melhoresTemposArray){

    hideLoading();

    $scope.data = melhoresTemposArray.map(function(mt){
          var obj = {};
          obj.idEstilo = mt.idEstilo;
          obj.nomeEstilo = mt.nomeEstilo;
          obj.melhorTempoFormatado = UserFriendlyService.fromMillisecondsToUserFriendlyTime(mt.tempo);
          obj.melhorTempo = mt.tempo;
          obj.hasTempo = mt.tempo > 0;
          return obj;
      });

  });

});