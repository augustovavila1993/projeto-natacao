angular.module('starter.controllers')

.controller('BestTimesEstimatesCtrl', function($scope, $stateParams, TimeEstimateService, UserFriendlyService) {

  $scope.nomeEstilo = $stateParams.nomeEstilo;
  var bestTimeInSeconds =  $stateParams.melhorTempo;

  // Gerar todas as distâncias (de 25 metros à 1500 aumentando 25 em 25)
  var distancesArr = [];
  for(var i = 25; i <= 1500; i += 25){
    distancesArr.push(i);
  }

  // Mapear a distância estimada para o tempo estimado para todas as distâncias do array distancesArr

  $scope.arrDistanceToTimeEstimates = distancesArr.map(function(distance){
    var obj = {};
    obj.distance = distance;
    obj.estimate = UserFriendlyService.fromMillisecondsToUserFriendlyTime(
                                          TimeEstimateService.estimate(bestTimeInSeconds, 100, distance)
    );
    return obj;
  });

});