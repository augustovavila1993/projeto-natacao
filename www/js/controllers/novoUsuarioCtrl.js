angular.module('starter.controllers')

.controller('NovoUsuarioCtrl', function($scope, $stateParams, $ionicPopup, $ionicLoading, $state, UserService, Config) {

  console.log("NovoUsuarioCtrl");

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  $scope.isAtleta = $stateParams.atleta == 1;

  $scope.addUser = function(user){

    if($stateParams.atleta && $stateParams.atleta == 1){
      user.isTecnico = false;
      console.log("user.isTecnico = false;");
    }else{
      user.isTecnico = true;
      console.log("user.isTecnico = true;");
    }

    $scope.user = user;
    console.log("$scope.user", $scope.user);
    
    var callback = function(result){

          console.log("Add user result callback: ", result);

          // Disable loading spinner
          hideLoading();

          if(result.status){

            if(Config.isAthlete()){
              $state.go('app.athlete_treino_menu');
              
            }else{

              $state.go('app.coach_treino_menu');

              /*
                Se for técnico, não podemos direcionar ele para dentro do app pois ele ainda
                  tem que pagar a taxa para ativar sua conta de técnico
              */
              //showAlert("Conta criada", "Muito obrigado por se cadastrar no nosso aplicativo. Agora você deve entrar em contato com nossa equipe pelo email appfilipina@gmail.com para realizar o pagamento da taxa de ativação da sua conta.");
            }
              
          }else{
            showAlert("Erro", result.message);
          }

          // redirecionar para a tela de Welcome
          $scope.status = result;
    };

    // Verificar se os campos foram preenchidos
    if(user.nome && user.email && user.senha){
      showLoading();

      setTimeout(function() {
          console.log("user", user);
          UserService.addUser(user, callback);
      }, 1000);

    }else{
        showAlert("Erro", "Preencha os campos do formulário");
    }

  };

});