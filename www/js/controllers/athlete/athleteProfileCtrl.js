angular.module('starter.controllers')

.controller('AthleteProfileCtrl', function($scope, $ionicPopup, $ionicLoading, NomesEventosService, Config, Athlete, UserService, AthleteService) {

  var showLoading = function(msg){
        $ionicLoading.show({
          template: msg
        });
      };

  var hideLoading = function(){
       $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  $scope.usuario = Config.getUsuario();
  $scope.atleta = Config.getAtleta();
  $scope.hasCoach = $scope.atleta.idAtletaTecnico > -1;

  /*
    Verificar no backend se este email realmente está associado a um usuário técnico. Se realmente for o email de um técnico,
    enviar uma solicitação de que este atleta quer registrá-lo como seu técnico no aplicativo
  */
  $scope.localizarTecnico = function(coachEmail){
    if(coachEmail){
        showLoading("Aguarde...");

        AthleteService.enviarSolicitacaoParaTecnico(coachEmail)
          .then(function(response){

              hideLoading();

              if(response.data.status){
                showAlert("Sucesso", "Solicitação enviada. Agora você deve aguardar a aprovação do seu técnico. \n\nQuando seu técnico te avisar que ele aceitou sua solicitação, por favor, feche o aplicativo e o abra novamente.");

              }else{

                var errorMsg = response.data.msg 
                                ? response.data.msg 
                                : "Técnico não encontrado. Verifique o e-mail digitado e tente novamente";
                showAlert("Erro", errorMsg);
              }

          }, function(err){
            hideLoading();
            console.log("err", err);
            alert("Ocorreu algum erro ao enviar solicitação para o técnico. Verifique sua conexão com a internet");
          });

    }else{
      showAlert("Erro", "Preencha o e-mail do seu técnico");
    }
  };

  $scope.saveProfile = function(atleta, usuario){

      showLoading("Aguarde...");

      AthleteService.updateAtleta(atleta, function(statusAtleta, msgAtleta){

          if(statusAtleta){

              UserService.updateUsuario(usuario, function(status, msg){
                  hideLoading();

                  if(status){
                      showAlert("Sucesso", msg);
                  }else{
                      showAlert("Erro", msg);
                  }
              });  

          }else{
              hideLoading();
              showAlert("Erro", msgAtleta);
          }

      });
  };

});
