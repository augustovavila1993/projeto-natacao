angular.module('starter.controllers')

.controller('AthleteCalendarioTreinosTreinoCtrl', function($scope, $ionicPopup, $ionicLoading, $state, $ionicModal, AthleteWorkoutService, AthleteService, TimeEstimateService, UserFriendlyService, IntensidadeService, EstiloService, Config) {

	var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
    };

	var hideLoading = function(){
      $ionicLoading.hide();
    };

	var showAlert = function(title, msg) {
	  var alertPopup = $ionicPopup.alert({
	    title: title,
	    template: msg
	  });
	};

	var formatarData = function(dateObj){
      var dia = dateObj.getDate() < 10 ? "0" + dateObj.getDate() : dateObj.getDate();
      var mes = dateObj.getMonth() + 1;
      mes = mes < 10 ? "0" + mes : mes;
      return "" + dia + "/" + mes + "/" + dateObj.getFullYear();
  	}

	$scope.enviarTreinoPorEmail = function(treino){
		console.log("enviarTreinoPorEmail treino", treino);

		// Assunto do e-mail
		var subject = "Treino: " + treino.treino.nome + " Data: " + formatarData(new Date(treino.treino.data));
		var to      = Config.getUsuario().email;

        if(window.plugins && window.plugins.emailComposer) {
            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                console.log("Response -> " + result);
            }, 
            subject, // Subject
            AthleteWorkoutService.formatarTreinoParaHtml(treino), // Body
            [to],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            null,                    // Attachments
            null);                   // Attachment Data

        }else{
        	alert("Plugin para envio de e-mail não foi encontrado");
        }
	};

	$scope.estilos = EstiloService.getEstilos();

	$ionicModal.fromTemplateUrl('opcoes_estilo.html', {
	    scope: $scope,
	    animation: 'slide-in-up'
	  }).then(function(modal) {
	    $scope.modal = modal
	  })

	$scope.abrirModalDeOpcoesDeEstilos = function(blocoSimples){
		$scope.modal.blocoSimples = blocoSimples;
		$scope.modal.show();
	};

	$scope.escolherOpcaoDeEstilo = function(blocoSimples, idEstiloEscolhido){
		blocoSimples.idEstilo = idEstiloEscolhido;
		$scope.modal.hide();
		$state.go($state.current, {}, {reload: true}); //second parameter is for $stateParams
	};

	$scope.getNomeIntensidade = IntensidadeService.getNomeIntensidade;
	$scope.getNomeEstilo = EstiloService.getNomeEstilo;
	$scope.formatSistemaEnergia = UserFriendlyService.formatSistemaEnergia;
	$scope.formatStyle = UserFriendlyService.formatStyle;
	$scope.formatTime = UserFriendlyService.fromSecondsToUserFriendlyTime;

	/*
		Retorno o melhor tempo do atleta para o estilo (idEstilo), em milisegundos
	*/
	var getMelhorTempoParaEstilo = function(listaMelhoresTempos, idEstilo){
		var melhorTempo = 0;

		if(listaMelhoresTempos && listaMelhoresTempos.length > 0){
			var arrayFiltrado = listaMelhoresTempos.filter(function(item){
				return item.idEstilo == idEstilo;
			});
			if(arrayFiltrado && arrayFiltrado.length > 0){
				var obj = arrayFiltrado.pop();
				melhorTempo = obj.tempo;
			}
		}

		return melhorTempo;
	};

	/*
		Retorna o treino com seus blocos ordenados
	*/
	var ordenarBlocos = function(treino){
		var blocos = treino.blocos;
		var nBlocos = blocos.length;

		if(treino && treino.blocos && treino.blocos.length > 0){

			var blocosOrdenadosAux = [];

			console.log("Antes de ordenar", treino.blocos);

			blocos.forEach(function(b){
				blocosOrdenadosAux[b.ordem - 1] = b;
			});

			treino.blocos.length = 0;

			blocosOrdenadosAux.forEach(function(b){
				treino.blocos.push(b);
			});

			console.log("Depois de ordenar", treino.blocos);
		}

		return treino;
	};

	var formatBlocoSimples = function(blocoSimples, listaMelhoresTempos){

		var idIntensidade = blocoSimples.idIntensidade;
		var distancia = blocoSimples.distancia;
		var idEstilo = blocoSimples.idEstilo;
		var melhorTempoEstiloEmSegundos = getMelhorTempoParaEstilo(listaMelhoresTempos, idEstilo) / 1000;
		var maxTime = TimeEstimateService.getTempoMaxASerFeito(idIntensidade, distancia, idEstilo, melhorTempoEstiloEmSegundos);

		blocoSimples.minTime =  TimeEstimateService.getTempoMinASerFeito(idIntensidade, distancia, idEstilo, melhorTempoEstiloEmSegundos);
		blocoSimples.maxTime = maxTime;
		blocoSimples.aCada = TimeEstimateService.getTiroACadaFormatado( 
			maxTime,
			TimeEstimateService.getInterval(idIntensidade, distancia) // intervalo da série
		);

		return blocoSimples;
	};

	var treino = AthleteWorkoutService.getTreinoAux();
	console.log("treino", treino);

	if(treino){

		showLoading();

		/*
			Buscar a lista de melhores tempos do atleta. É com base
				nesta lista que a estimativa de tempos para cada série do 
				treino é feita
		*/
		AthleteService.getMelhoresTempos(function(){
			hideLoading()
			showAlert("Erro", "Erro ao buscar lista de melhores tempos do atleta");

		}, function(listaMelhoresTempos){

			hideLoading();

			// Estimar quanto o atleta tem que fazer em cada série do treino
			treino.blocos = treino.blocos.map(function(bloco){

				if(bloco.isBlocoSimples){
					bloco.blocoSimples = formatBlocoSimples(bloco, listaMelhoresTempos);

				}else{
					// É bloco composto
					bloco.blocos = bloco.blocos.map(function(blocoSimples){
						return formatBlocoSimples(blocoSimples, listaMelhoresTempos);
					});
				}
				
				return bloco;
			});

			$scope.treino              = ordenarBlocos(treino);
			$scope.dataFormatadaTreino = formatarData(new Date(treino.treino.data));
		});

	}else{
		console.log("AthleteCalendarioTreinosTreinoCtrl", "não encontrou o treino");
	}

});