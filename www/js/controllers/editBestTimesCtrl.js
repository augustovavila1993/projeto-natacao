angular.module('starter.controllers')

.controller('EditBestTimesCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $ionicPopup, $stateParams, AthleteService, UserFriendlyService, Config) {

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  // new Date(year, month, day, hours, minutes, seconds, milliseconds)
  var melhorTempoDate = new Date(0, 0, 0, 0, 0, 0, $stateParams.melhorTempo);
  $scope.minutes = melhorTempoDate.getMinutes();
  $scope.seconds = melhorTempoDate.getSeconds();
  $scope.cen = (melhorTempoDate.getMilliseconds() / 10) | 0;
  $scope.nomeEstilo = $stateParams.nomeEstilo;
  var idEstilo = $stateParams.idEstilo;

  $scope.saveBestTime = function(min, sec, cen){
    
      // Validar se os segudos estao entre 0 e 59
      if(sec < 0 || sec > 59){
        showAlert("Erro", "Campo segundos deve estar entre 0 e 59");

      }else if(cen < 0 || cen > 99){
        showAlert("Erro", "Campo centésimos deve estar entre 0 e 99");        

      }else{

          showLoading();
          var tempoEmMill = ( min * 60 * 1000 ) + (sec * 1000) + (cen * 10);

          AthleteService.updateMelhorTempo(idEstilo, tempoEmMill).then(
              function(response){

                  hideLoading();

                  if(response.data.status){
                      $state.go('app.best_times');

                  }else{
                    showAlert("Erro", response.data.msg);
                  }

              }, function(err){
                  console.log("Erro ao editar o melhor tempo", err);
                  hideLoading();
                  showAlert("Erro", "Ocorreu algum erro ao salvar o tempo inserido. Tente novamente mais tarde.");
          });

      }

  } // fim do método saveBestTime


});