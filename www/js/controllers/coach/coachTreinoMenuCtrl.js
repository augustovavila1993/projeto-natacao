angular.module('starter.controllers')

.controller('CoachTreinoMenuCtrl', function($scope, $timeout, $ionicLoading, $state, $ionicHistory, CoachWorkoutService, UserService) {

	$scope.novoTreino = function(){
		CoachWorkoutService.setTreinoAux(undefined);
		$state.go('app.coach_treino_menu_novo_treino');
	};

	$scope.logout = function(){
		
		$ionicLoading.show({template:'Logging out....'});
	    UserService.setUsuarioLogado("", "");

	    $timeout(function () {

	          $ionicLoading.hide();
	          $ionicHistory.clearCache();
	          $ionicHistory.clearHistory();
	          $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
	          console.log("CoachTreinoMenuCtrl", "Fazendo logout");
	          $state.go('login');

	    }, 100);

	};

});