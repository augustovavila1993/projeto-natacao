angular.module('starter.controllers')

/*
	Buscar o treino do técnico que tem este id e mostrá-lo na tela
*/
.controller('CoachCalendarioTreinosTreinoCtrl', function($scope, $ionicHistory, $ionicPopup, $ionicLoading, $state, EstiloService, IntensidadeService, CoachWorkoutService, UserFriendlyService, Config) {

	$scope.getNomeIntensidade   = IntensidadeService.getNomeIntensidade;
	$scope.getNomeEstilo        = EstiloService.getNomeEstilo;
	$scope.formatSistemaEnergia = UserFriendlyService.formatSistemaEnergia;
	$scope.formatStyle          = UserFriendlyService.formatStyle;
	$scope.formatTime           = UserFriendlyService.fromSecondsToUserFriendlyTime;

	var showLoading = function(){
	    $ionicLoading.show({
	      template: 'Aguarde...'
	    });
	};

	var hideLoading = function(){
	    $ionicLoading.hide();
	};

	var showAlert = function(title, msg) {
	  var alertPopup = $ionicPopup.alert({
	    title: title,
	    template: msg	
	  });
	};

	var formatarData = function(dateObj){
      var dia = dateObj.getDate() < 10 ? "0" + dateObj.getDate() : dateObj.getDate();
      var mes = dateObj.getMonth() + 1;
      mes = mes < 10 ? "0" + mes : mes;
      return "" + dia + "/" + mes + "/" + dateObj.getFullYear();
  	}

	$scope.editarTreino = function(treino){
		alert("Funcionalidade ainda não implementada");
	};

	$scope.enviarTreinoPorEmail = function(treino){
		console.log("enviarTreinoPorEmail treino", treino);

		// Assunto do e-mail
		var subject = "Treino: " + treino.treino.nome + " Data: " + formatarData(new Date(treino.treino.data));
		var to      = Config.getUsuario().email;

        if(window.plugins && window.plugins.emailComposer) {
            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                console.log("Response -> " + result);
            }, 
            subject, // Subject
            CoachWorkoutService.formatarTreinoParaHtml(treino), // Body
            [to],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            null,                    // Attachments
            null);                   // Attachment Data

        }else{
        	alert("Plugin para envio de e-mail não foi encontrado");
        }
	};

	$scope.excluirTreino = function(idTreino){

		var confirmPopup = $ionicPopup.confirm({
	     	title: 'Excluir Treino',
	     	template: 'Tem certeza que deseja excluir este treino?'
	    });

	    confirmPopup.then(function(res) {

	      if(res) {

	        console.log('Deletar o treino ' + idTreino);
	      	showLoading();

	        CoachWorkoutService.excluirTreino(idTreino, function(erro){

	        	hideLoading();
	        	showAlert("Erro", "Erro ao excluir o treino");

	        }, function(){
	        	
	        	hideLoading();
	        	/*
					Redirecionar o técnico para a tela anterior
	        	*/
	        	$ionicHistory.goBack();
	        });

	      }
	    });
	};


	/*
		Retorna o treino com seus blocos ordenados
	*/
	var ordenarBlocos = function(treino){
		var blocos = treino.blocos;
		var nBlocos = blocos.length;

		if(treino && treino.blocos && treino.blocos.length > 0){

			var blocosOrdenadosAux = [];

			console.log("Antes de ordenar", treino.blocos);

			blocos.forEach(function(b){
				blocosOrdenadosAux[b.ordem - 1] = b;
			});

			treino.blocos.length = 0;

			blocosOrdenadosAux.forEach(function(b){
				treino.blocos.push(b);
			});

			console.log("Depois de ordenar", treino.blocos);
		}

		return treino;
	};

	var treino = CoachWorkoutService.getTreinoAux();
	console.log("treino", treino);

	if(treino){
		$scope.treino = ordenarBlocos(treino);
		$scope.dataFormatadaTreino = formatarData(new Date(treino.treino.data));

	}else{
		console.log("Não encontrou o treino");
	}

});