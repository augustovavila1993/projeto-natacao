angular.module('starter.controllers')

.controller('CoachCalendarioTreinosCtrl', function($scope, $state, $ionicPopup, $ionicLoading, $ionicModal, CoachWorkoutService, UserFriendlyService) {

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  $scope.goToTreino = function(treino){
    console.log("goToTreino treino:", treino);
    CoachWorkoutService.setTreinoAux(treino);

    /*
      Antes de ir para pa tela do treino, salvar a data base da semana que está sendo visualizada
        para que quando o usuário volta para o calendário seja possível manter o contexto.
    */
    CoachWorkoutService.setWeekReferenceDate($scope.referenceDate);
    $state.go('app.coach_treino_menu_calendario_treinos_treino', {workoutId: treino.treino.id});
  };

  $ionicModal.fromTemplateUrl('opcoes_treino.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  }) 

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  
  $scope.goToNextWeek = function(){
      $scope.referenceDate.setDate($scope.referenceDate.getDate() + 7);
      initPage($scope.referenceDate);
  };

  $scope.goToPreviousWeek = function(){
      $scope.referenceDate.setDate($scope.referenceDate.getDate() - 7);
      initPage($scope.referenceDate);
  };

  /*
    Recebe como parâmetro um array de objetos treino
    Irá ordenar os treinos de acordo com a sua data de criação
  */
  var ordenarTreinosPorDataCriacao = function(treinosArr){
    console.log("ordenarTreinosPorDataCriacao");

    return treinosArr.sort(function(treinoA, treinoB){
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      console.log("treinoB.treino.dataCriado: " + treinoB.treino.dataCriado);
      console.log("treinoA.treino.dataCriado: " + treinoA.treino.dataCriado);
      return new Date(treinoA.treino.dataCriado) - new Date(treinoB.treino.dataCriado);
    });
  }

  $scope.viewWorkout = function(treinosArr){
    // show loading dialog
    console.log("viewWorkout treinosArr: ", treinosArr);

    if(treinosArr.length == 1){
      var treino = treinosArr.pop();
      $scope.goToTreino(treino);
    
    }else if(treinosArr.length > 1){
      // Mostrar modal na tela pro técnico escolher qual treino ele quer visualizar
      $scope.modal.treinos = ordenarTreinosPorDataCriacao(treinosArr);
      $scope.modal.show();
    }
  };

  /*
    Com base na data fromDate, retornar um objeto que represente o primeiro dia desta semana
  */
  var getWeekFirstDayDate = function(fromDate){
    fromDate.setDate(fromDate.getDate() - fromDate.getDay());
    return fromDate;
  }

  /*
    Com base na data fromDate, retornar o último dia desta semana
  */
  var getWeekLastDayDate = function(fromDate){
    fromDate.setDate(fromDate.getDate() + (6 - fromDate.getDay()) );
    return fromDate;
  };

  $scope.formatarData = function(dateObj){
      var dataFormatada = "-";

      if(dateObj){
        var dia = dateObj.getDate() < 10 ? "0" + dateObj.getDate() : dateObj.getDate();
        var mes = dateObj.getMonth() + 1;
        mes = mes < 10 ? "0" + mes : mes;  
        dataFormatada = "" + dia + "/" + mes + "/" + dateObj.getFullYear();
      }
      
      return dataFormatada;
  }

  /*
    Retorna true se o treino tiver a mesma data
  */
  var verificaTreinoNaData = function(treino, dataObj){
    var dataTreino = new Date(treino.treino.data);

    return dataTreino.getDate() == dataObj.getDate()
        && dataTreino.getMonth() == dataObj.getMonth()
        && dataTreino.getFullYear() == dataObj.getFullYear();
  }

  /*
    Usa como referência para montar a tela a data refDate

    $scope.referenceDate deve estar setado com a data de referência para a semana que 
       se quer visualizar
  */
  var initPage = function(){

      //alert($scope.referenceDate);
      
      //alert("getWeekFirstDayDate($scope.referenceDate): " + getWeekFirstDayDate($scope.referenceDate));
      //alert("getWeekLastDayDate($scope.referenceDate).getDate(): " + getWeekLastDayDate($scope.referenceDate));

      showLoading();

      /*
        Objeto weekInfo contém todas as informações da semana
      */
      var weekInfo = {
        firstDay : getWeekFirstDayDate($scope.referenceDate).getDate(),
        lastDay : getWeekLastDayDate($scope.referenceDate).getDate(),
        monthWritten : UserFriendlyService.getMonthWrittenForm(getWeekLastDayDate($scope.referenceDate).getMonth()).monthAbreviated,
        weekDaysWorkoutInfo : [],
        isSemanaAtual : false
      };

      // Verificar se o usuário está visualizando a semana atual
      if(new Date().getTime() > getWeekFirstDayDate($scope.referenceDate).getTime() && new Date().getTime() < getWeekLastDayDate($scope.referenceDate).getTime()){
          weekInfo.isSemanaAtual = true;
      }

      var fromDate = getWeekFirstDayDate($scope.referenceDate);
      var fromDateStr = fromDate.toJSON();
      var toDate   = getWeekLastDayDate($scope.referenceDate);
      var toDateStr = toDate.toJSON();

      CoachWorkoutService.getMeusTreinos(fromDateStr, toDateStr, function(erro){

          hideLoading();
          showAlert("Erro", "Erro ao buscar os treinos");

      }, function(treinos){

          console.log("Agrupar os treinos por dia da semana, treinos: ", treinos);

          /*
            Criar os dias da semana
          */ 
          for(var i = 0; i < 7; i++){
            var weekDayInfo = {
              isToday : false
            };

            weekDayInfo.weekDayWritten = UserFriendlyService.getWeekDayWritten(i);

            var weekFirstDayDate = getWeekFirstDayDate($scope.referenceDate);
            weekFirstDayDate.setDate(weekFirstDayDate.getDate() + i);

            weekDayInfo.isToday = weekFirstDayDate.getDate() == new Date().getDate() && weekFirstDayDate.getMonth() == new Date().getMonth();
            //weekDayInfo.day = weekFirstDayDate.getDate() < 10 ? "0" + weekFirstDayDate.getDate() : weekFirstDayDate.getDate();
            weekDayInfo.day = weekFirstDayDate.getDate();
            var mes = weekFirstDayDate.getMonth() + 1;
            //weekDayInfo.month = mes < 10 ? "0" + mes : mes;
            weekDayInfo.month = weekFirstDayDate.getMonth() + 1;
            weekDayInfo.treinos = [];

            treinos

              /*
                Filtar os treinos que são para este dia da semana
              */
              .filter(function(treino){
                return verificaTreinoNaData(treino, weekFirstDayDate);
              })

              // Para cada treino filtrado, adicionar o id dele no treinos das informações deste dia da semana
              .forEach(function(treinoDesteDia){
                weekDayInfo.treinos.push(treinoDesteDia);
              }); 

              console.log("weekDayInfo", weekDayInfo);
              weekInfo.weekDaysWorkoutInfo.push(weekDayInfo);
          }

          $scope.weekInfo     = weekInfo;
          $scope.weekFirstDay = getWeekFirstDayDate($scope.referenceDate);
          hideLoading();

      });
  };

  $scope.referenceDate = CoachWorkoutService.getWeekReferenceDate() != undefined
                              ? CoachWorkoutService.getWeekReferenceDate()
                              : new Date();

  initPage();
  

});