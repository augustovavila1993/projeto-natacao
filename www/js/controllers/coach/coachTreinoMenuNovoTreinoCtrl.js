angular.module('starter.controllers')

.controller('CoachTreinoMenuNovoTreinoCtrl', function($scope, $state, $ionicLoading, $ionicActionSheet, $ionicModal, $ionicPopup, $ionicHistory, Config, UserFriendlyService, rx, CoachWorkoutService, EstiloService, IntensidadeService) {

  console.log("CoachTreinoMenuNovoTreinoCtrl");

  var showLoading = function(){
      $ionicLoading.show({
        template: 'Aguarde...'
      });
  };

  var hideLoading = function(){
      $ionicLoading.hide();
  };

  var showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

 $scope.showActionSheetBloco = function(bloco) {

    console.log("showActionSheetBloco", bloco);

   // Show the action sheet
   var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: '<i class="icon ion-share balanced"></i> Editar' }
     ],
     titleText: 'Menu',
     cancelText: 'Cancelar',
     destructiveText: '<i class="icon ion-trash-a assertive"></i> Remover',

     cancel: function() {},

     buttonClicked: function(index) {
      if(index == 0){ // Editar um bloco simples ou bloco composto

          if(bloco.isBlocoSimples){
            $scope.openModalEditSerie(bloco);

          }else{

            /*
              Mandar usuário para a tela de criar/editar bloco composto
              Vou passar o parâmetro editarBloco com o valor para para indicar que o bloco
                composto será editado. E passar a ordem deste bloco no treino para que no próximo
                comtroller seja posível pegar este bloco para mostrá-lo na tela.
            */
            CoachWorkoutService.setTreinoAux($scope.treino);
            $state.go('app.coach_treino_bloco_composto', {editarBloco: true, ordem : bloco.ordem});
          }
      }
       return true;
     },

     destructiveButtonClicked: function(){
      console.log("Vai deletar o bloco clicado");
      removerBloco(bloco);
      return true;
     }
   });
 };

 var calcularDistanciaBlocoComposto = function(blocoComposto){
    return blocoComposto.repeticoes * blocoComposto.blocos.map(function(blocoSimples){

            return calcularDistanciaBlocoSimples(blocoSimples);

        }).reduce(function(distanciaAcumulada, distancia, index, seriesArray){

            return distanciaAcumulada + distancia // somar todas as distâncias das séries

        }, 0);
  };

  var calcularDistanciaBlocoSimples = function(blocoSimples){
    return blocoSimples.series * blocoSimples.repeticoes * blocoSimples.distancia;
  };

 /*
    Calcular a distância total do treino (em metros)
  */
  var atualizarDistanciaTreino = function(){
      
      $scope.treino.treino.distanciaTotal = $scope.treino.blocos
          .map(function(bloco){

              if(bloco.isBlocoSimples){
                  return calcularDistanciaBlocoSimples(bloco);

              }else{
                return calcularDistanciaBlocoComposto(bloco);
              }

          })
          .reduce(function(distanciaAcumulada, distancia, index, seriesArray){
            return distanciaAcumulada + distancia // somar todas as distâncias das séries
          }, 0);
  };

  $scope.getNomeEstilo = EstiloService.getNomeEstilo;
  $scope.getNomeIntensidade = IntensidadeService.getNomeIntensidade;
  $scope.estilos = EstiloService.getEstilos();
  $scope.intensidades = IntensidadeService.getIntensidades();
  $scope.treino = CoachWorkoutService.getTreinoAux() ? CoachWorkoutService.getTreinoAux() : {
    treino : {
        data : "",
        nome : "",
        distanciaTotal : 0
    },
    blocos : []
  };

  atualizarDistanciaTreino();  

  $ionicModal.fromTemplateUrl('nova_serie.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal
  });

  /*
    Salva o treino temporariamente no Service para poder passar o treino pra a tela de criação
      de série composta
  */
  $scope.novoBlocoComposto = function(){
    CoachWorkoutService.setTreinoAux($scope.treino);

    /*
      Ir para o controller que permite a criação de um novo bloco composto
    */
    $state.go('app.coach_treino_bloco_composto', {editarBloco: false, ordem : 0});
  };

  $scope.openNovaSerieModal = function() {
    $scope.modal.novaSerie = true;
    $scope.modal.show();
  };

  // Usado para editar uma série
  $scope.openModalEditSerie = function(serie) {
    $scope.modal.novaSerie = false;
    $scope.modal.data = serie;
    $scope.modal.show();
  };

  var validarSerie = function(serie){
    console.log("validarSerie");
    console.log("serie", serie);

    if(serie.series && serie.repeticoes && serie.distancia && serie.idEstilo && serie.idIntensidade){
      return true;
    }else{
      return false;
    }
  };

  var removerBloco = function(bloco){
    console.log("Bloco a ser removido", bloco);
    if(bloco){
      
      $scope.treino.blocos = $scope.treino.blocos.filter(function(blocoDoFilter){
        return blocoDoFilter.ordem != bloco.ordem;
      });

      atualizarDistanciaTreino();
    }
  };

  // Tanto adicionar uma nova série quando editar uma que já exista
  $scope.addOrEditSerie = function(isNovaSerie, data) {

    console.log("addOrEditSerie");
    console.log("data", data);

    if(validarSerie(data)){

      if(isNovaSerie){

          var novoBlocoSimples = {
            isBlocoSimples : true,
            ordem          : $scope.treino.blocos.length + 1,
            idEstilo       : data.idEstilo,
            idIntensidade  : data.idIntensidade,
            series         : data.series,
            repeticoes     : data.repeticoes,
            distancia      : data.distancia
          };

          $scope.treino.blocos.push(novoBlocoSimples);
      }

      atualizarDistanciaTreino();
      $scope.modal.data = {};
      $scope.modal.hide();

    }else{
      showAlert("Erro", "Preencha todos os campos");
    }
    
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });


  $scope.finalizarTreino = function(treino){
    console.log("finalizarTreino");
    console.log("treino", treino);

    /*
      TODO: validar o treino
    */
    if(treino.treino.data === undefined || treino.treino.data.length == 0 
          || treino.treino.nome === undefined || treino.treino.nome.length == 0){
      
        showAlert("Erro", "Campos Nome e Data são obrigatórios");

    }else{

        showLoading();

        /*
          Formatar a data do treino para o formato datetime
        */
        if(treino.treino.data){
          var dateObjAux = new Date(treino.treino.data);
          var mes = dateObjAux.getMonth() + 1;
          treino.treino.data = dateObjAux.getFullYear() + "-" + mes + "-" + dateObjAux.getDate() + " 12:00:00";
        }

        CoachWorkoutService
              .postTreino(treino)
              .then(

                  function(resposta){
                    console.log("resposta", resposta);
                    hideLoading();

                    if(resposta.data.status){

                      showAlert("Sucesso", "Treino criado");
                      CoachWorkoutService.setTreinoAux(undefined);
                      $ionicHistory.goBack();

                    }else{
                      showAlert("Erro", resposta.data.msg);
                    }
                  },

                  function(err){
                      hideLoading();
                      showAlert("Erro", "Ocorreu um erro ao salvar o treino. Tente novamente mais tarde");
                  }
              );

    }

  };

});