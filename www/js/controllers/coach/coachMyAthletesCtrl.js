angular.module('starter.controllers')

.controller('CoachMyAthletesCtrl', function($scope, $ionicLoading, $ionicPopup, Config, MyAthletesService) {

	var showLoading = function(){
	    $ionicLoading.show({
	      template: 'Aguarde...'
	    });
  	};

	var hideLoading = function(){
	    $ionicLoading.hide();
	};

	var showAlert = function(title, msg) {
	  var alertPopup = $ionicPopup.alert({
	    title: title,
	    template: msg
	  });
	};

	/*
		TODO:

		- criar botão para atualizar esta página
		- ok buscar atletas confirmados
		- ok ação para confirmar atleta
		- ok ação para recusar um atleta
	*/

	var buscarAtletasPendentes = function(){

		$scope.requestAtletasPendentesFinalizada = false;

		MyAthletesService
			.getAtletasPendentesDoTecnico()
			.then(function(response){
				$scope.requestAtletasPendentesFinalizada = true;
				console.log("response atletas pendentes", response);

				if(response.data.status){
					$scope.atletasPendentes = response.data.atletas;

				}else{
					console.log("Erro ao buscar os atletas pendentes deste técnico");
					// TODO: mostrar mensagem de erro
				}

			}, function(error){
				$scope.requestAtletasPendentesFinalizada = true;
				console.log("error", error);
				// TODO: mostrar mensagem de erro
			});
	};

	var buscarAtletasConfirmados = function(){
		console.log("buscarAtletasConfirmados");

		MyAthletesService
			.getAtletasConfirmadosDoTecnico()
			.then(function(response){
				console.log("response atletas pendentes", response);

				if(response.data.status){
					$scope.atletasConfirmados = response.data.atletas;

				}else{
					console.log("Erro ao buscar os atletas confirmados deste técnico");
					// TODO: mostrar mensagem de erro
				}

			}, function(error){
				console.log("error", error);
				console.log("Erro ao buscar os atletas confirmados deste técnico");
				// TODO: mostrar mensagem de erro
			});
	};

	$scope.aceitar = function(idAtleta){

		console.log("aceitar idAtleta:", idAtleta);
		showLoading();
		
		MyAthletesService.aceitarSolicitacaoDoAtleta(idAtleta)
			.then(

				function(response){
					hideLoading();

					if(response.data.status){
						showAlert("Ok", "Solicitação confirmada");
						init();
					}else{
						showAlert("Erro", "Tente novamente mais tarde");
					}
				}, 

				function(err){
					console.log("err", err);
					hideLoading();
					showAlert("Erro", "Tente novamente mais tarde");
				}
			);
	};

	$scope.negarSolicitacaoDoAtleta = function(idAtleta){
		console.log("negarSolicitacaoDoAtleta idAtleta", idAtleta);

		var confirmPopup = $ionicPopup.confirm({
	     	title: 'Excluir atleta',
	     	template: 'Tem certeza que não quer mais ser o técnico deste atleta?'
	    });

	    confirmPopup.then(function(res) {

	      if(res) {

	      	showLoading();
		
			MyAthletesService.recusarSolicitacaoDoAtleta(idAtleta)
				.then(

					function(response){
						hideLoading();

						if(response.data.status){
							init();

						}else{
							showAlert("Erro", "Tente novamente mais tarde");
						}
					}, 

					function(err){
						console.log("err", err);
						hideLoading();
						showAlert("Erro", "Tente novamente mais tarde");
					}
				);

	      }
	    });
	};

	/*
		Função que irá inicializar esta tela
		Buscar atletas pendentes e atletas já confirmados por este técnico
	*/
	var init = function(){
		buscarAtletasConfirmados();
		buscarAtletasPendentes();
	};

	// Inicar o processamento para criar os dados desta dela
	init();
});
