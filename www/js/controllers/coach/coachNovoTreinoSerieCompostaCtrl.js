angular.module('starter.controllers')

.controller('CoachNovoTreinoSerieCompostaCtrl', function($scope, $state, $ionicHistory, $ionicModal, $ionicActionSheet, $ionicPopup, UserFriendlyService, CoachWorkoutService, EstiloService, IntensidadeService) {

	console.log("CoachWorkoutService.getTreinoAux()", CoachWorkoutService.getTreinoAux());
	$scope.treino = CoachWorkoutService.getTreinoAux();

	/*
		Parâmetros que virão na quando este controller for chamado
	*/
	console.log("$state.params", $state.params);
	var editarBlocoParam = $state.params.editarBloco;
	console.log("editarBlocoParam");
	var ordemParam = $state.params.ordem;
	console.log("ordemParam", ordemParam);

	if(editarBlocoParam == "true"){
		console.log("Editar bloco composto");
		console.log("editarBlocoParam", editarBlocoParam);

		/*
			Localizar o bloco que o usário está querendo editar
		*/
		$scope.blocoComposto = $scope.treino.blocos.filter(function(blocoDoFilter){
			return blocoDoFilter.ordem == ordemParam;
		}).pop();

	}else{

		console.log("Novo bloco composto");

		$scope.blocoComposto = {
			repeticoes  : 0,
			intervalo   : 0,
			blocos      : []
		};
	}

	$scope.getNomeEstilo = EstiloService.getNomeEstilo;
  	$scope.getNomeIntensidade = IntensidadeService.getNomeIntensidade;
  	$scope.estilos = EstiloService.getEstilos();
  	$scope.intensidades = IntensidadeService.getIntensidades();

	console.log("$scope.treino", $scope.treino);

	var removerBloco = function(bloco){
	    console.log("Bloco a ser removido", bloco);
	    if(bloco){
	      
	      $scope.treino.blocos = $scope.treino.blocos.filter(function(blocoDoFilter){
	        return blocoDoFilter.ordem != bloco.ordem;
	      });

	      atualizarDistanciaTreino();
	    }
  	};

  	$ionicModal.fromTemplateUrl('nova_serie.html', {
	    scope: $scope,
	    animation: 'slide-in-up'
	  }).then(function(modal) {
	    $scope.modal = modal
  	});

  	$scope.openNovaSerieModal = function() {
	    $scope.modal.novaSerie = true;
	    $scope.modal.show();
  	};

  	// Usado para editar uma série
  	$scope.openModalEditSerie = function(serie) {
	    $scope.modal.novaSerie = false;
	    $scope.modal.data = serie;
	    $scope.modal.show();
  	};

	$scope.showActionSheetBloco = function(bloco) {

	    console.log("showActionSheetBloco", bloco);

	   // Show the action sheet
	   var hideSheet = $ionicActionSheet.show({
	     buttons: [
	       { text: '<i class="icon ion-share balanced"></i> Editar' }
	     ],
	     titleText: 'Menu',
	     cancelText: 'Cancelar',
	     destructiveText: '<i class="icon ion-trash-a assertive"></i> Remover',

	     cancel: function() {},

	     buttonClicked: function(index) {
	      if(index == 0){ // Editar um bloco simples
	        $scope.openModalEditSerie(bloco);
	      }
	       return true;
	     },

	     destructiveButtonClicked: function(){
	      console.log("Vai deletar o bloco clicado");
	      removerBloco(bloco);
	      return true;
	     }
	   });
	 };


	$scope.finalizarSerieComposta = function(blocoComposto){
		console.log("Finalizar bloco composto", blocoComposto);

		var treino = $scope.treino;

		if(editarBlocoParam != "true"){

			var bloco = {
				ordem          : treino.blocos.length + 1, // ordem dessa série no treino,
				repeticoes     : blocoComposto.repeticoes,
				isBlocoSimples : false,
				intervalo      : blocoComposto.intervalo,
				blocos         : blocoComposto.blocos
			};

			treino.blocos.push(bloco);
		}
		
		CoachWorkoutService.setTreinoAux(treino);
		$ionicHistory.goBack();
	};

	var showAlert = function(title, msg) {
	    var alertPopup = $ionicPopup.alert({
	      title: title,
	      template: msg
	    });
	};

	$scope.openNovaSerieModal = function() {
	    $scope.modal.novaSerie = true;
	    $scope.modal.show();
	};

	$ionicModal.fromTemplateUrl('nova_serie.html', {
	    scope: $scope,
	    animation: 'slide-in-up'
	}).then(function(modal) {
	    $scope.modal = modal
	});

	// Tanto adicionar uma nova série quando editar uma que já exista
	$scope.addOrEditSerie = function(isNovaSerie, data) {

		console.log("isNovaSerie", isNovaSerie);
		console.log("addOrEditSerie data", data);

	    if(validarSerie(data)){

	      if(isNovaSerie){

	          var newObj = {};
	          newObj.series = data.series;
	          newObj.repeticoes = data.repeticoes;
	          newObj.distancia = data.distancia;
	          newObj.idEstilo = data.idEstilo;
	          newObj.idIntensidade = data.idIntensidade;
	          newObj.ordem = $scope.blocoComposto.blocos.length + 1;

	          $scope.blocoComposto.blocos.push(newObj);
	      }

	      $scope.modal.data = {};
	      $scope.modal.hide();

	    }else{
	      showAlert("Erro", "Preencha todos os campos");
	    } 

	}; // Fim do método addOrEditSerie


	$scope.$on('$destroy', function() {
	    $scope.modal.remove();
	});

	var validarSerie = function(serie){
	    if(serie.series && serie.repeticoes && serie.distancia && serie.idEstilo && serie.idIntensidade){
	      return true;
	    }else{
	      return false;
	    }
	};

	var removerBloco = function(bloco){
	    if(bloco){
	      $scope.treino.blocos = $scope.treino.blocos.filter(function(blocoDoFilter){
	        return blocoDoFilter.ordem != bloco.ordem;
	      });
	    }
	};

	$scope.showActionSheetSerie = function(bloco) {

		   // Show the action sheet
		   var hideSheet = $ionicActionSheet.show({
		     buttons: [
		       { text: '<i class="icon ion-share balanced"></i> Editar' }
		     ],
		     titleText: 'Menu',
		     cancelText: 'Cancelar',
		     destructiveText: '<i class="icon ion-trash-a assertive"></i> Remover',

		     cancel: function() {},

		     buttonClicked: function(index) {
		      if(index == 0){ // Editar o bloco
		          $scope.openModalEditSerie(bloco);
		      }
		       return true;
		     },

		     destructiveButtonClicked: function(){
		      removerBloco(bloco);
		      return true;
		     }
		   });

	}; // Fim do método showActionSheetSerie

});