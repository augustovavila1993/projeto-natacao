angular.module('starter.controllers')

.controller('EscolherSouAtletaOuTecnicoCtrl', function($scope, $state) {  

  $scope.souAtleta = function(){
      $state.go('novo_usuario', {atleta: 1});
  };

  $scope.souTecnico = function(){
    $state.go('novo_usuario', {atleta: 0});
  };

});