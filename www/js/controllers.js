angular.module('starter.controllers', ['rx'])

.controller('AppCtrl', function($scope, Config) {

  var isCoach = Config.getUsuario().isTecnico;
  $scope.profile_hash = isCoach ? "coach_profile" : "athlete_profile";
  $scope.treino_menu_hash = isCoach ? "coach/treino_menu" : "athlete/treino_menu";
  $scope.userIsCoach = isCoach;
  
})


.controller('AthleteTreinoMenuCtrl', function($scope, $ionicLoading, $state, $ionicHistory, $timeout, UserService) {
  console.log("AthleteTreinoMenuCtrl");

  	$scope.logout = function(){

      $ionicLoading.show({template:'Logging out....'});
      UserService.setUsuarioLogado("", "");

      $timeout(function () {

          $ionicLoading.hide();
          $ionicHistory.clearCache();
          $ionicHistory.clearHistory();
          $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
          console.log("AthleteTreinoMenuCtrl", "Fazendo logout");
          $state.go('login');

      }, 100);

	  };
})


.controller('CoachTreinoMenuCtrl', function(Config, $scope) {
  console.log("CoachTreinoMenuCtrl");

  $scope.coach = Config.getTecnico();
})  


.controller('TreinosCtrl', function($scope) {
})


.controller('TesteCtrl', function($scope) {
});