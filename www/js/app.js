console.log("entrou");


// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngResource'])

.run(function($ionicPlatform, Config, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    
    if(window.StatusBar){
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  });

})

/*
  Usar sempre o layout do IOS porque é mais bonito
*/
.config(function($ionicConfigProvider) {
    $ionicConfigProvider.views.transition('ios');
    $ionicConfigProvider.tabs.style('standard').position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center').positionPrimaryButtons('left');
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'AppCtrl'
  })


  // --------------------------- Configurações de Perfil ---------------------------

  .state('app.athlete_profile', {
    url: '/athlete_profile',
    views: {
      'tab_athlete_profile': {
        templateUrl: 'templates/athlete/athlete_profile.html',
        controller: 'AthleteProfileCtrl'
      }
    }
  })

  .state('app.coach_profile', {
    url: '/coach_profile',
    views: {
      'tab_coach_profile': {
        templateUrl: 'templates/coach/coach_profile.html',
        controller: 'CoachProfileCtrl'
      }
    }
  })


  /*
    Aba do menu de treino do ATLETA
  */
  .state('app.athlete_treino_menu', {
    url: '/athlete/treino_menu',
    cache: false,
    views: {
      'tab_athlete_treino': {
        templateUrl: 'templates/athlete/athlete_treino_menu.html',
        controller: 'AthleteTreinoMenuCtrl'
      }
    }
  })

  /*
    Calendário de treinos do ATLETA
  */
  .state('app.athlete_treino_menu_calendario_treinos', {
    url: '/athlete/treino_menu/calendario_treinos',
    cache: false,
    views: {
      'tab_athlete_treino': {
        templateUrl: 'templates/athlete/athlete_calendario_treinos.html',
        controller: 'AthleteCalendarioTreinosCtrl'
      }
    }
  })

  /*
    Visualizaçao de um treino do Atleta
  */
  .state('app.athlete_treino_menu_calendario_treinos_treino', {
    url: '/athlete/treino_menu/calendario_treinos/:workoutId',
    cache: false,
    views: {
      'tab_athlete_treino': {
        templateUrl: 'templates/athlete/athlete_calendario_treinos_treino.html',
        controller: 'AthleteCalendarioTreinosTreinoCtrl'
      }
    }
  })


  /*
    Menu da aba de treino do TÉCNICO
  */
  .state('app.coach_treino_menu', {
    url: '/coach/treino_menu',
    cache: false,
    views: {
      'tab_coach_treino': {
        templateUrl: 'templates/coach/coach_treino_menu.html',
        controller: 'CoachTreinoMenuCtrl'
      }
    }
  })

  /*
    Calendário de semana em semana dos treinos do TÉCNICO
  */
  .state('app.coach_calendario_treinos', {
      url: '/coach/treino_menu/calendario_treinos',
      cache: false,
      views: {
        'tab_coach_treino': {
          templateUrl: 'templates/coach/coach_calendario_treinos.html',
          controller: 'CoachCalendarioTreinosCtrl'
        }
      }
  })


  /*
    TÉCNICO visualiza um treino que ele criou
  */
  .state('app.coach_treino_menu_calendario_treinos_treino', {
    url: '/coach/treino_menu/calendario_treinos/:workoutId',
    cache: false,
    views: {
      'tab_coach_treino': {
        templateUrl: 'templates/coach/coach_treino_menu_calendario_treinos_treino.html',
        controller: 'CoachCalendarioTreinosTreinoCtrl'
      }
    }
  })


  .state('app.coach_treino_menu_novo_treino', {
    url: '/coach/treino_menu/novo_treino',
    cache: false,
    views: {
      'tab_coach_treino': {
        templateUrl: 'templates/coach/coach_treino_menu_novo_treino.html',
        controller: 'CoachTreinoMenuNovoTreinoCtrl'
      }
    }
  })

  // Novo Treino - tela para criar bloco composto de séries

  .state('app.coach_treino_bloco_composto', {
      url: '/coach/treino_menu/novo_treino/bloco_composto/:editarBloco/:ordem',
      cache: false,
      views: {
        'tab_coach_treino': {
          templateUrl: 'templates/coach/coach_novo_treino_bloco_composto.html',
          controller: 'CoachNovoTreinoSerieCompostaCtrl'
        }
      }
    })
  
  
  /*
      Atletas do Técnico
  */
  .state('app.coach_my_athletes', {
    url: '/coach/my_athletes',
    cache: false,
    views: {
      'tab_coach_my_athletes': {
        templateUrl: 'templates/coach/my_athletes.html',
        controller: 'CoachMyAthletesCtrl'
      }
    }
  })




// --------------------------- Melhores Tempos ---------------------------

  .state('app.best_times', {
    url: '/best_times',
    cache: false,
    views: {
      'tab_best_times': {
        templateUrl: 'templates/best_times.html',
        controller: 'BestTimesCtrl'
      }
    }
  })

  .state('app.edit_best_time', {
    url: '/best_times/edit/:idEstilo/:nomeEstilo/:melhorTempo',
    cache: false,
    views: {
      'tab_best_times': {
        templateUrl: 'templates/edit_best_time.html',
        controller: 'EditBestTimesCtrl'
      }
    }
  })

  // Estimativas dos tempos do atleta
  .state('app.best_times_estimates', {
    url: '/best_times/estimates/:nomeEstilo/:melhorTempo',
    cache: false,
    views: {
      'tab_best_times': {
        templateUrl: 'templates/best_times_estimates.html',
        controller: 'BestTimesEstimatesCtrl'
      }
    }
  })


  // --------------------------- Painel de Atividades ---------------------------

  .state('app.dashboard', {
    url: '/dashboard',
    views: {
      'tab_dashboard': {
        templateUrl: 'templates/dashboard.html',
        controller: 'DashboardCtrl'
      }
    }
  })

  // ---------------------------- Página para testes
  .state('app.teste', {
    url: '/teste',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/teste.html',
        controller: 'TesteCtrl'
      }
    }
  })


  // --------------------------- Login / Novo Usuário ---------------------------

  .state('escolher_sou_atleta_ou_tecnico', {
    url: '/escolher_sou_atleta_ou_tecnico',
    templateUrl: 'templates/escolher_sou_atleta_ou_tecnico.html',
    controller: 'EscolherSouAtletaOuTecnicoCtrl'
  })

  .state('novo_usuario', {
    url: '/novo_usuario/:atleta',
    templateUrl: 'templates/novo_usuario.html',
    controller: 'NovoUsuarioCtrl'
  })

  .state('login', {
      url: "/login",
      templateUrl: "templates/login.html",
      controller: 'LoginCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});

