angular.module('starter.services')

.factory('User', function($resource, Config) {
  var relativeUrl = '/api/user/:email';
  var absoluteUrl = Config.getServerUrl() + relativeUrl;

    var data = $resource(absoluteUrl, {email : "@email"}, {
      update:{
          method:'PUT'
      }
    });

    return data;
})


/**
	Para atualizar um atleta, posso usar o método update. Exemplo: 
    Atleta.update({email: "augusto@teste.com"}, {apelido: 'Guto', email: 'guto@novoemail.com'});
*/
.factory('Athlete', function($resource, Config) {
	var relativeUrl = '/api/athlete/:email';
	var absoluteUrl = Config.getServerUrl() + relativeUrl;

  	var data = $resource(absoluteUrl, {email : "@email"}, {
      update:{
          method:'PUT'
      }
    });
    return data;
})


.factory('Coach', function($resource, Config) {
	var relativeUrl = '/api/coach/:email';
	var absoluteUrl = Config.getServerUrl() + relativeUrl;

  	var data = $resource(absoluteUrl, {email : "@email"}, {
      update:{
          method:'PUT'
      }
    });
    return data;
})


.factory('CoachWorkout', function($http, Config) {
  
  var data = {'product': $scope.product, 'product2': $scope.product2 };
  var coach = Config.getTecnico();
  var url = Config.getServerUrl() + "/api/coach/" + coach.email + "/workout";

  return rx.Observable.fromPromise( $http.post( url, data) );

});