angular.module('starter.services')

.factory('UserFriendlyService', function(){

	var estilos  = {
		estilo : "Estilo",
		crawl : "Crawl",
		butterfly : "Borboleta",
		backstroke : "Costas",
		breaststroke : "Peito",
		medley : "Medley",

		bracoEstilo : "Braço Estilo",
		bracoCrawl  : "Braço Crawl",
		bracoButterfly  : "Braço Borboleta",
		bracoBackstroke  : "Braço Costas",
		bracoBreaststroke  : "Braço Peito",

		pernaEstilo : "Perna Estilo",
		pernaCrawl : "Perna Crawl",
		pernaButterfly : "Perna Borboleta",
		pernaBackstroke : "Perna Costas",
		pernaBreaststroke : "Perna Peito",

		pernaMaisPeDePato : "Perna + Pé de Pato",
		palmar : "Palmar",
		palmarMaisPeDePato : "Palmar + Pé de Pato",

		paraquedasPequenoEstilo : "Paraquedas Pequeno Estilo",
		paraquedasPequenoCrawl : "Paraquedas Pequeno Crawl",
		paraquedasPequenoButterfly : "Paraquedas Pequeno Borboleta",
		paraquedasPequenoBackstroke : "Paraquedas Pequeno Costas",
		paraquedasPequenoBreaststroke : "Paraquedas Pequeno Peito",

		paraquedasMedioEstilo : "Paraquedas Médio Estilo",
		paraquedasMedioCrawl : "Paraquedas Médio Crawl",
		paraquedasMedioButterfly : "Paraquedas Médio Borboleta",
		paraquedasMedioBackstroke : "Paraquedas Médio Costas",
		paraquedasMedioBreaststroke : "Paraquedas Médio Peito",

		paraquedasGrandeEstilo : "Paraquedas Grande Estilo",
		paraquedasGrandeCrawl : "Paraquedas Grande Crawl",
		paraquedasGrandeButterfly : "Paraquedas Grande Borboleta",
		paraquedasGrandeBackstroke : "Paraquedas Grande Costas",
		paraquedasGrandeBreaststroke : "Paraquedas Grande Peito",

		submerso : "Submerso"
	};

	return {
		formatSistemaEnergia : function(sistemaEnergia){
			var sisEnergiaFormatado = "";

			switch(sistemaEnergia){
				case 'a0' : sisEnergiaFormatado = 'A0';
					break;
				case 'a1' : sisEnergiaFormatado = 'A1';
					break;
				case 'a2' : sisEnergiaFormatado = 'A2';
					break;
				case 'a3' : sisEnergiaFormatado = 'A3';
					break;
				case 'resan' : sisEnergiaFormatado = 'RESAN';
					break;
				case 'tl' : sisEnergiaFormatado = 'TL';
					break;
				case 'pt' : sisEnergiaFormatado = 'PT';
					break;
				case 'vl' : sisEnergiaFormatado = 'VL';
					break;
			}

			return sisEnergiaFormatado;
		},

		// Formata o estilo (key) para um formato legível para ser humano. peDePato => Pé de Pato
		formatStyle : function(styleKey){
			return estilos[styleKey] ? estilos[styleKey] : "Não definido";
		},

		// Retorna uma lista (array) com todos as keys dos estilos: [ crawl, butterfly, ... ]
		getEstilosKeys : function(){
			var keys = [];
			for(var estiloKey in estilos){
				keys.push(estiloKey);
			}
			return keys;
		},

		getEstilos : function(){
			return estilos;
		},

		// Crawl, borboleta, costas e peito
		getQuatroEstilos : function(){
			var quatroEstilos = {};
			quatroEstilos.crawl = estilos["crawl"];
			quatroEstilos.butterfly = estilos["butterfly"];
			quatroEstilos.backstroke = estilos["backstroke"];
			quatroEstilos.breaststroke = estilos["breaststroke"];
			return quatroEstilos;
		},

		fromSecondsToUserFriendlyTime : function(timeInSeconds){
			//var time = new Date(year, month, day, hours, minutes, seconds, milliseconds);
			var time = new Date(0, 0, 0, 0, 0, timeInSeconds, 0);
		    var auxMin = time.getMinutes();
		    var auxSeg = time.getSeconds();
		    minStr = auxMin < 10 ? "0" + auxMin : "" + auxMin;
		    segStr = auxSeg < 10 ? "0" + auxSeg : "" + auxSeg;
		    return minStr + ":" + segStr; 
		},

		fromMillisecondsToUserFriendlyTime : function(timeInMilli){
			//var time = new Date(year, month, day, hours, minutes, seconds, milliseconds);
			var time = new Date(0, 0, 0, 0, 0, 0, timeInMilli);
		    var auxMin = time.getMinutes();
		    var auxSeg = time.getSeconds();
		    var auxCen = ( time.getMilliseconds() / 10 ) | 0;

		    var minStr = auxMin < 10 ? "0" + auxMin : "" + auxMin;
		    var segStr = auxSeg < 10 ? "0" + auxSeg : "" + auxSeg;
		    var cenStr = auxCen < 10 ? "0" + auxCen : "" + auxCen;

		    return minStr + "\'" + segStr + "\"" + cenStr;
		},

		getTimeInSeconds : function(minutes, seconds){
		    return ( minutes * 60 ) + seconds;
		},

		formatTime : function(min, sec){
			var timeFormatted = "";
			timeFormatted += min < 10 ? "0" + min : min;
			timeFormatted += " : ";
			timeFormatted += sec < 10 ? "0" + sec : sec;
			return timeFormatted;
		},

		/*
			retorna um objeto {monthAbreviated : "", month : ""}
		*/
		getMonthWrittenForm : function(monthInt){
			var monthAbreviated = "";
			var month = "";

			switch(monthInt){
				case 0 :
					monthAbreviated = "Jan";
					month = "Janeiro";
					break;
				case 1 :
					monthAbreviated = "Fev";
					month = "Fevereiro";
					break;
				case 2 :
					monthAbreviated = "Mar";
					month = "Março";
					break;
				case 3 :
					monthAbreviated = "Abr";
					month = "Abril";
					break;
				case 4 :
					monthAbreviated = "Mai";
					month = "Maio";
					break;
				case 5 :
					monthAbreviated = "Jun";
					month = "Junho";
					break;
				case 6 :
					monthAbreviated = "Jul";
					month = "Julho";
					break;
				case 7 :
					monthAbreviated = "Ago";
					month = "Agosto";
					break;
				case 8 :
					monthAbreviated = "Set";
					month = "Setembro";
					break;
				case 9 :
					monthAbreviated = "Out";
					month = "Outubro";
					break;
				case 10 :
					monthAbreviated = "Nov";
					month = "Novembro";
					break;
				case 11 :
					monthAbreviated = "Dez";
					month = "Dezembro";
					break;
			}

			return {
				monthAbreviated : monthAbreviated,
				month : month
			};
		},

		/*
			retorna Segunda se vier 0 (zero) no parâmetro
		*/
		getWeekDayWritten : function(weekDayInt){
			var weekday = "";

			switch(weekDayInt){
				case 0 : 
					weekday = "Domingo";
					break;
				case 1 : 
					weekday = "Segunda";
					break;
				case 2 : 
					weekday = "Terça";
					break;
				case 3 : 
					weekday = "Quarta";
					break;
				case 4 : 
					weekday = "Quinta";
					break;
				case 5 : 
					weekday = "Sexta";
					break;
				case 6 : 
					weekday = "Sábado";
					break;
			}

			return weekday;
		}

	};
});