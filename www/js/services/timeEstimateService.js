angular.module('starter.services')

.factory('TimeEstimateService', function(Config, $http, EndPoint){

	// <=100m;  > 100m < 400m; >=400m

	var intervaloPorIntensidade = [];

	/*
	var intervals = [
		['a0', 30, 45, 60], // a0
		['a1', 10, 20, 30], // a1
		['a2', 20, 30, 45], // a2
		['a3', 60, 75, 90], // a3
		['resan', 120, 150, 180], // resan
		['tl', 180, 240, 300], // tl
		['pt', 360, 480, 600], // pt
		['vl', 0, 0, 0] // vl
	];
	*/

	var intensidadeZonaTreinamentoFraco = [];
	/*
	var intensidadeZonaTreinamentoFraco = [
		['a0', 1.5, 1.44, 1.33],
		['a1', 1.4, 1.29, 1.23],
		['a2', 1.3, 1.19, 1.13],
		['a3', 1.2, 1.14, 1.1],
		['resan', 1.08, 1.07, 1.06],
		['tl', 1.07, 1.06, 1.05],
		['pt', 1.05, 1.04, 1.03],
		['vl', 1.14, 0, 0]
	];
	*/

	var intensidadeZonaTreinamentoForte = [];
	/*
	var intensidadeZonaTreinamentoForte = [
		['a0', 1.45, 1.4, 1.35],
		['a1', 1.35, 1.25, 1.2],
		['a2', 1.25, 1.15, 1.1],
		['a3', 1.15, 1.1, 1.07],
		['resan', 1.03, 1.05, 1.04],
		['tl', 1.02, 1.04, 1.03],
		['pt', 1, 1.01, 1.02],
		['vl', 1.05, 0, 0]
	];
	*/

	var publicInterface = {

		/*
			Busca os registros do banco de dados da tabela intervalo_por_intensidade
				e guarda eles na variável intervaloPorIntensidade deste serviço
		*/
		getAndRefreshIntervaloPorIntensidade : function(errorCallback, successCallback){
			console.log("getAndRefreshIntervaloPorIntensidade");
			var promise = $http.get( EndPoint.getListaIntervaloPorIntensidade() );

			promise.then(function(result){
				console.log("result ", result);
				console.log("result.data.intervaloPorIntensidade", result.data.intervaloPorIntensidade);
				publicInterface.setIntervaloPorIntensidadeArray(result.data.intervaloPorIntensidade);
				successCallback();

			}, function(err){
				console.log("Erro", err);
				errorCallback(err);
			});
		},

		setIntervaloPorIntensidadeArray : function(inputArray){
			console.log("setIntervaloPorIntensidadeArray");
			intervaloPorIntensidade.length = 0;

			if(inputArray && inputArray.length > 0){
				inputArray.forEach(function(item){
					intervaloPorIntensidade.push(item);
				});
			}

			console.log("Depois do setIntervaloPorIntensidadeArray", intervaloPorIntensidade);
		},

		// ---------------- Zona de Treinamento Fraco ----------------------------------------------------------

		/*
			Busca os registros do banco de dados da tabela intensidade_zona_treinamento_fraco
				e guarda eles na variável intensidadeZonaTreinamentoFraco deste serviço 
		*/
		getAndRefreshIntensidadeZonaTreinamentoFraco : function(errorCallback, successCallback){
			console.log("getAndRefreshIntensidadeZonaTreinamentoFraco"); 
			var promise = $http.get( EndPoint.getListaIntensidadeZonaTreinamentoFraco() );

			promise.then(function(result){
				console.log("result ", result);
				console.log("result.data.intervaloZonaTreinamentoFraco", result.data.intervaloZonaTreinamentoFraco);
				publicInterface.setIntervaloZonaTreinamentoFracoArray(result.data.intervaloZonaTreinamentoFraco);
				successCallback();

			}, function(err){
				console.log("Erro", err);
				errorCallback(err);
			});
		},

		setIntervaloZonaTreinamentoFracoArray : function(inputArray){
			console.log("setIntervaloZonaTreinamentoFracoArray");
			intensidadeZonaTreinamentoFraco.length = 0;

			if(inputArray && inputArray.length > 0){
				inputArray.forEach(function(item){
					intensidadeZonaTreinamentoFraco.push(item);
				});
			}

			console.log("Depois do setIntervaloZonaTreinamentoFracoArray", intensidadeZonaTreinamentoFraco);
		},


		// ---------------- Zona de Treinamento Forte -------------------------------------------------------

		/*
			Busca os registros do banco de dados da tabela intensidade_zona_treinamento_forte
				e guarda eles na variável intensidadeZonaTreinamentoForte deste serviço 
		*/
		getAndRefreshIntensidadeZonaTreinamentoForte : function(errorCallback, successCallback){
			console.log("getAndRefreshIntensidadeZonaTreinamentoForte"); 
			var promise = $http.get( EndPoint.getListaIntensidadeZonaTreinamentoForte() );

			promise.then(function(result){
				console.log("result ", result);
				console.log("result.data.intervaloZonaTreinamentoForte", result.data.intervaloZonaTreinamentoForte);
				publicInterface.setIntervaloZonaTreinamentoForteArray(result.data.intervaloZonaTreinamentoForte);
				successCallback();

			}, function(err){
				console.log("Erro", err);
				errorCallback(err);
			});
		},

		setIntervaloZonaTreinamentoForteArray : function(inputArray){
			console.log("setIntervaloZonaTreinamentoForteArray");
			intensidadeZonaTreinamentoForte.length = 0;

			if(inputArray && inputArray.length > 0){
				inputArray.forEach(function(item){
					intensidadeZonaTreinamentoForte.push(item);
				});
			}

			console.log("Depois do setIntervaloZonaTreinamentoForteArray", intensidadeZonaTreinamentoForte);
		},

		// -------------------------------------------------------------------------------------------------


		/*
			Tempo Estimado = (tempo real) * ( (distância estimada) / (distância real) )^1.09
		*/
		estimate : function(realTimeInMilliseconds, realDistanceInMeters, estimatedDistanceInMeters){
			var constEsforco = 0;

			if(estimatedDistanceInMeters <= 100){
				constEsforco = 1.15;
			}else if( estimatedDistanceInMeters <= 200){
				constEsforco = 1.14;
			}else if( estimatedDistanceInMeters <= 300){
				constEsforco = 1.13;
			}else if( estimatedDistanceInMeters <= 400){
				constEsforco = 1.12;
			}else if( estimatedDistanceInMeters <= 500){
				constEsforco = 1.11;
			}else if( estimatedDistanceInMeters <= 700){
				constEsforco = 1.10;
			}else if( estimatedDistanceInMeters <= 1000){
				constEsforco = 1.09;
			}else if( estimatedDistanceInMeters <= 1400){
				constEsforco = 1.08;
			}else if( estimatedDistanceInMeters <= 1500){
				constEsforco = 1.07;
			}

			var result =  realTimeInMilliseconds * Math.pow( (estimatedDistanceInMeters/realDistanceInMeters), constEsforco  );
			return Math.round(result)
		},

		/*
		forte - boolean - se for true irá usar os dados da zona de treinamento forte,
				caso contrário, irá usar a zona fraca
		*/
		getIntensidadeZonaTreinamento : function(idIntensidade, opcaoMetragem, forte){

			if(forte){
				return this.getIntensidadeZonaTreinamentoForte(idIntensidade, opcaoMetragem);
			}else{
				return this.getIntensidadeZonaTreinamentoFraco(idIntensidade, opcaoMetragem);
			}
		},

		getTimeInSeconds : function(min, sec){
			return (min * 60) + sec;
		},

		getBestTime : function(idEstilo){
			console.log("idEstilo", idEstilo);
			var bestTimes = Config.getAtleta().bestTimes;
			return this.getTimeInSeconds(bestTimes[estiloKey].minutes, bestTimes[estiloKey].seconds);
		},

		/*
			idIntensidade: id da intensidade registrado no banco de dados
			melhorTempoEmSeg: melhor tempo do atleta para a intensidade em segundos
			distancia: em metros
			idEstilo: id do estilo registrado no banco de dados
			minimo: boolean - se for true, retornará o tempo mínimo a ser feito. Caso contrário, o tempo máximo

			return: Tempo mínimo a ser feito em segundos
		*/
		getTempoASerFeito : function(idIntensidade, melhorTempoEmSeg, distancia, idEstilo, minimo){
			var realDistanceInMeters = 100;
			var tempoEstimadoParaADistancia = this.estimate(melhorTempoEmSeg, realDistanceInMeters, distancia);

			var opcaoMetragem = -1;
			if(distancia <= 100){
				opcaoMetragem = 1;
			}else if(distancia > 100 && distancia < 400){
				opcaoMetragem = 2;
			}else{
				opcaoMetragem = 3;
			}

			var constanteIntensidade = 
						minimo ? this.getIntensidadeZonaTreinamentoForte(idIntensidade, opcaoMetragem)
								: this.getIntensidadeZonaTreinamentoFraco(idIntensidade, opcaoMetragem);

			return tempoEstimadoParaADistancia * constanteIntensidade;
		},

		/*
			idIntensidade: id da intensidade registrado no banco de dados
			distancia: em metros
			estilo: crawl, butterfly, backstroke, breaststroke

			return: Tempo mínimo a ser feito em segundos
		*/
		getTempoMinASerFeito : function(idIntensidade, distancia, estilo, melhorTempoEstiloEmSegundos){
			return this.getTempoASerFeito(idIntensidade, melhorTempoEstiloEmSegundos,  distancia, estilo, true);
		},

		/*
			idIntensidade: id da intensidade registrado no banco de dados
			distancia: em metros
			idEstilo: id do estilo registrado no banco de dados

			return: Tempo mínimo a ser feito em segundos
		*/
		getTempoMaxASerFeito : function(idIntensidade, distancia, idEstilo, melhorTempoEstiloEmSegundos){
			/*
				Buscar o melhor tempo de 100 metros do atleta para o estilo idEstilo
			*/


			return this.getTempoASerFeito(idIntensidade, melhorTempoEstiloEmSegundos, distancia, idEstilo, false);
		},

		/*
			Retorna o tempo em segundos do intervalo

			idIntensidade: id da intensidade registrado no banco de dados
			
		*/
		getInterval : function(idIntensidade, distanciaMetros){

			/*
				opcaoMetragem: 
					1 - <= 100;
					2 - > 100m < 400m
					3 - >=400m
			*/
			var opcaoMetragem = 0;

			if(distanciaMetros <= 100){
				opcaoMetragem = 1;
			}else if(distanciaMetros > 100 && distanciaMetros < 400){
				opcaoMetragem = 2;
			}else{
				opcaoMetragem = 3;
			}

			var intervaloArr = intervaloPorIntensidade.filter(function(item){
				return item.idIntensidade == idIntensidade
			});

			var intervalo = 0;

			if(intervaloArr && intervaloArr.length > 0){
				var objIntervaloIntensidade = intervaloArr.pop();
				if(opcaoMetragem == 1){
					intervalo = objIntervaloIntensidade.intervaloUm;
				}else if(opcaoMetragem == 2){
					intervalo = objIntervaloIntensidade.intervaloDois;
				}else{
					intervalo = objIntervaloIntensidade.intervaloTres;
				}
			}

			return intervalo;
		},

		/*
			maximoTempoDoTiro (em segundos) - de acordo com o cálculo de quanto o atleta tem que fazer em um tiro, este é o máximo que ele pode fazer
			para se manter dentro do quadro de melhor performance possível dele

			intervalo (em segundos) - quanto de intervalo o atleta deve fazer entre os tiros da série em questão

			Exemplo: Se o tempo máximo que o atleta pode fazer em um tiro de 200 livre é 2min10 e o intervalo entre os tiros da série
			é de 30 segundos, este método irá trazer A CADA 2min40 (2min10 + 30). Serão feitas aproximações de 5 em 5 segundos para facilitar
			o entendimento e clareza de quanto A CADA quanto tempo o atleta deve fazer o tiro da série.
		*/
		getTiroACada : function(maximoTempoDoTiro, intervalo){
			console.log("intervalo", intervalo);
			console.log("maximoTempoDoTiro", maximoTempoDoTiro);
			return maximoTempoDoTiro + intervalo;
		},

		getTiroACadaFormatado : function(maximoTempoDoTiro, intervalo){

			console.log("Cálculo A Cada");
			console.log("maximoTempoDoTiro", maximoTempoDoTiro);
			console.log("intervalo", intervalo);

			var tiroACadaEmSegundos = this.getTiroACada(maximoTempoDoTiro, intervalo);
			console.log("tiroACadaEmSegundos", tiroACadaEmSegundos);
			
			var aux = tiroACadaEmSegundos / 60;
		    // Pegar a parte inteira do float
		    var auxMin = aux | 0;
		    var auxSeg = aux - auxMin;
		    auxSeg *= 60;

		    if(auxSeg % 10 < 5){
				auxSeg += 5 - (auxSeg % 10);
			}else if(auxSeg % 10 > 5){
				auxSeg += 10 - (auxSeg % 10);
			}

			if(auxSeg == 60){
				auxMin += 1;
				auxSeg = 0;
			}

		    // Pegar a parte inteira do float
		    auxSeg = auxSeg | 0;
		    minStr = auxMin < 10 ? "0" + auxMin : "" + auxMin;
		    segStr = auxSeg < 10 ? "0" + auxSeg : "" + auxSeg;
		    return minStr + ":" + segStr;
		},

		/**
			idIntensidade: id da intensidade registrado no banco de dados
			opcaoMetragem: 1 - <= 100;
							2 - > 100m < 400m
							3 - >=400m
		*/
		getIntensidadeZonaTreinamentoForte : function(idIntensidade, opcaoMetragem){

			var valorIntensidadeZonaTreinForte = 0;

			var arrayFiltrado = intensidadeZonaTreinamentoForte.filter(function(item){
				return item.idIntensidade == idIntensidade;
			});

			if(arrayFiltrado && arrayFiltrado.length > 0){

				var obj = arrayFiltrado.pop();

				if(opcaoMetragem == 1){
					valorIntensidadeZonaTreinForte = obj.intervaloUm;

				}else if(opcaoMetragem == 2){
					valorIntensidadeZonaTreinForte = obj.intervaloDois;

				}else{
					valorIntensidadeZonaTreinForte = obj.intervaloTres;
				}
			}

			return valorIntensidadeZonaTreinForte;
		},

		/**
			idIntensidade: id da intensidade registrado no banco de dados
			opcaoMetragem: 1 - <= 100;
							2 - > 100m < 400m
							3 - >=400m
		*/
		getIntensidadeZonaTreinamentoFraco : function(idIntensidade, opcaoMetragem){
			
			var valorIntensidadeZonaTreinFraco = 0;

			var arrayFiltrado = intensidadeZonaTreinamentoFraco.filter(function(item){
				return item.idIntensidade == idIntensidade;
			});

			if(arrayFiltrado && arrayFiltrado.length > 0){

				var obj = arrayFiltrado.pop();

				if(opcaoMetragem == 1){
					valorIntensidadeZonaTreinFraco = obj.intervaloUm;

				}else if(opcaoMetragem == 2){
					valorIntensidadeZonaTreinFraco = obj.intervaloDois;

				}else{
					valorIntensidadeZonaTreinFraco = obj.intervaloTres;
				}
			}

			return valorIntensidadeZonaTreinFraco;
		}

	};

	return publicInterface;
});