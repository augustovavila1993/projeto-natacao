angular.module('starter.services')

.factory('UserService', function(Athlete, Coach, User, Config, EndPoint, $http){

	var publicInterface = {

		verificaUsuarioLogado : function(){
			console.log("window.localStorage.getItem email", window.localStorage.getItem("email"));
			console.log("window.localStorage.getItem senha", window.localStorage.getItem("senha"));
			return window.localStorage.getItem("email") !== null && window.localStorage.getItem("senha") !== null;
	  	},

		getEmailUsuarioLogado : function(){
		    return window.localStorage.getItem("email");
		},

		getSenhaUsuarioLogado : function(){
		    return window.localStorage.getItem("senha");
		},

		setUsuarioLogado : function(email, senha){
			console.log("email", email);
			console.log("senha", senha);

			window.localStorage.setItem("email", email);
			window.localStorage.setItem("senha", senha);
		},
		
		addUser : function(user, callback){
			
			var resultCallback = {
	          status : true,
	          message : "Usuário cadastrado"
	      	};

	      	if(user.hasOwnProperty('isTecnico') && user.isTecnico){

	      		var dadosNovoUsuario = {
	      			nome      : user.nome,
	      			email     : user.email,
	      			senha     : user.senha,
	      			isTecnico : user.isTecnico
	      		};

	      		$http.post( EndPoint.novoUsuario(), dadosNovoUsuario ).then(function(response){

	      			console.log("Resposta do POST de Novo Usuário", response);

	      			if(response.data.status && response.data.usuario){

	      				Config.setUsuario(response.data.usuario);
	      				if(response.data.usuario.isTecnico){
	      					Config.setTecnico(response.data.tecnico);
	      				}else{
	      					Config.setAtleta(response.data.atleta);
	      				}

	      			}else{
	      				resultCallback.status = false;
	      				resultCallback.message = response.data.msg;
	      			}

	      			callback(resultCallback);
	      			
	      		}, function(err){
	      			resultCallback.status = false;
	      			resultCallback.message = "Erro ao cadastrar usuário";
	      			callback(resultCallback);
	      		});


	      	}else{
	      		
	      		console.log("Cadastro de novo atleta");

	      		var dadosNovoUsuario = {
	      			nome      : user.nome,
	      			email     : user.email,
	      			senha     : user.senha,
	      			isTecnico : user.isTecnico ? user.isTecnico : false
	      		};

	      		console.log("dadosNovoUsuario", dadosNovoUsuario);

	      		$http.post( EndPoint.novoUsuario(), dadosNovoUsuario ).then(function(response){

	      			console.log("Resposta do POST de Novo Usuário", response);

	      			if(response.data.status && response.data.usuario){

	      				Config.setUsuario(response.data.usuario);
	      				if(response.data.usuario.isTecnico){
	      					Config.setTecnico(response.data.tecnico);
	      				}else{
	      					Config.setAtleta(response.data.atleta);
	      				}

	      			}else{
	      				resultCallback.status = false;
	      				resultCallback.message = response.data.msg;
	      			}

	      			callback(resultCallback);
	      			
	      		}, function(err){
	      			resultCallback.status = false;
	      			resultCallback.message = "Erro ao cadastrar usuário";
	      			callback(resultCallback);
	      		});
		      		
	      	}
			
		}, // End of the method addUser

		/*
			callback recebe dois parâmetros. Primeiro o status e depois a msg
		*/
		updateUsuario : function(usuario, callback){
			$http.post(
				EndPoint.updateUsuario(),
				{usuario: usuario}
			).then(function(response){

				if(response.data.status){
					Config.setUsuario(usuario);
					callback(true, response.data.msg);

				}else{
					console.log("Erro ao atualizar o atleta", err);
					callback(false, response.data.msg);	
				}

			}, function(err){
				console.log("Erro ao atualizar o atleta", err);
				callback(false, "Erro ao atualizar o atleta");
			});
		}
	};

	return publicInterface;
});