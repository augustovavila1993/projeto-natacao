angular.module('starter.services')

.factory('AuthService', function($http, EndPoint, Athlete, Coach, User, Config, rx){
	return {

		autenticar2 : function(email, password){

			return rx.Observable.create(function(observer){

	      		var usuarioAutenticadoMsg = "Usuário autenticado"
	      		var usuarioNaoEncontradoMsg = "Usuário não encontrado";
	      		var senhaIncorretaMsg = "Senha incorreta";

	      		User.get({email: email}, function(data){
	      	
			      	if(data.userFound){
			      		console.log("userFound", data.userFound);

						if(data.user.password == password){
							console.log("senha correta");

							if(data.user.isCoach){
								console.log("usuário é TÉCNICO");

								Coach.get({email: email}, function(coachData){

									if(coachData.coachFound){
										Config.setSessionUser(data.user, coachData.coach);
										observer.onNext({success : true, message : usuarioAutenticadoMsg});
									}else{
										observer.onNext({success : false, message : usuarioNaoEncontradoMsg});
									}

									observer.onCompleted();

								}, function(err){
									observer.onNext({success : false, message : senhaIncorretaMsg});
									observer.onCompleted();
								});

							}else{
								console.log("usuário é ATLETA");

								Athlete.get({email: email}, function(athleteData){

									if(athleteData.athleteFound){

										Config.setSessionUser(data.user, athleteData.athlete);

										if(athleteData.athlete.coachEmail){

												console.log("com técnico");

												// Buscar o técnico deste atleta
												Coach.get({email: athleteData.athlete.coachEmail},
													function(coachData){
														if(coachData.coachFound){
															athleteData.athlete.coachWorkouts = coachData.coach.workouts;

															Config.setSessionUser(data.user, athleteData.athlete);
							      							observer.onNext({success : true, message : usuarioAutenticadoMsg});
							      							observer.onCompleted();
														}
													}, 

													function(err){
														console.log("erro", err);
														observer.onNext({success : false, message : "Erro ao buscar os dados do técnico"});
														observer.onCompleted();
													}
												);
												// Fim da busca do técnico deste atleta

										}else{
											console.log("sem técnico");
											observer.onNext({success : true, message : usuarioAutenticadoMsg});

											// Este atleta não tem técnico
											observer.onCompleted();
										}

										
									}else{
										console.log("erro x");
									    observer.onNext({success : false, message : usuarioNaoEncontradoMsg});
									}

								}, function(err){
									console.log("erro y");
									observer.onNext({success : false, message : senhaIncorretaMsg});
									observer.onCompleted();
								});
							}
							
						}else{
							observer.onNext({success : false, message : senhaIncorretaMsg});
				        	observer.onCompleted();
				        }      		
			      	}else{
			      		observer.onNext({success : false, message : usuarioNaoEncontradoMsg});
				        observer.onCompleted();
				  	}
			      
			      }, function(err){
			      		console.log("Erro ao fazer a requisição ao servidor")
			      		observer.onNext({success : false, message : "Caiu no callback de erro"});
			            observer.onCompleted();
				});

			});
			
		}, // fim método autenticar


		autenticar : function(email, senha, callback){

			var responseCallbackObj = {
				status : true,
				msg    : "Usuário autenticado",
			};

			var dadosPost = {
      			email : email,
      			senha : senha
      		};

      		$http.post( EndPoint.autenticarUsuario(), dadosPost ).then(function(response){

      			console.log("Resposta do POST de autenticação de usuário", response);

      			if(response.data.status && response.data.usuario){

      				Config.setUsuario(response.data.usuario);
      				if(response.data.usuario.isTecnico){
      					Config.setTecnico(response.data.tecnico);
      				}else{
      					Config.setAtleta(response.data.atleta);
      				}

      			}else{
      				responseCallbackObj.status = false;
      				responseCallbackObj.msg    = response.data.msg;
      			}

      			callback(responseCallbackObj);
      			
      		}, function(err){
      			responseCallbackObj.status = false;
      			responseCallbackObj.msg = "E-mail ou senha incorretos";
      			callback(responseCallbackObj);
      		});
		}
	}
});