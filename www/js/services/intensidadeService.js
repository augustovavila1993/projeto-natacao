angular.module('starter.services')

.factory('IntensidadeService', function($http, Config, EndPoint){

	/*
		É um array que guarda objetos de intensidade que seguem o formato:
		{
			id : "0", // id da intensidade que foi cadastrado no backend
			nome : "nome teste" // nome da intensidade cadastrado no backend
		}
	*/
	var intensidades = [];

	var publicInterface = {

		getIntensidades : function(){
			return intensidades;
		},

		setIntensidades : function(intensidadesArray){
			console.log("setIntensidades intensidadesArray", intensidadesArray);
			intensidades.length = 0;

			if(intensidadesArray && intensidadesArray.length > 0){
				intensidadesArray.forEach(function(intensidadeInput){
					intensidades.push(intensidadeInput);
				});
			}

			console.log("Depois do Set", intensidades);
		},

		getNomeIntensidade : function (id){

			if(intensidades.length > 0){

				return intensidades.filter(function(intensidade){
					return intensidade.id == id;
				}).pop().nome;

			}else{
				return "Intensidade não encontrada";
			}
		},

		getIdIntensidade : function(nomeIntensidade){
			if(intensidades.length > 0){

				return intensidades.filter(function(intensidade){
					return intensidade.nome.toLowerCase() == nomeIntensidade.toLowerCase();
				}).pop().id;

			}else{
				return "Intensidade não encontrada";
			}
		},

		/*
			Fará uma requisição GET para buscar a lista de intensidades 
				cadastradas no backend

			Depois de buscar a lista de intensidades, inseri-las no atributo deste Service
		*/
		getAndRefreshIntensidadesListFromServer : function(errorCallback, successCallback){
			var intensidadesPromise = $http.get( EndPoint.getListaDeIntensidades() );

			intensidadesPromise.then(function(result){
				console.log("result - lista de intensidades", result);
				console.log("intensidades", result.data.intensidades);
				publicInterface.setIntensidades(result.data.intensidades);
				successCallback();

			}, function(err){
				console.log("Erro ao buscar lista de intensidades", err);
				errorCallback(err);
			});
		}
	};

	return publicInterface;
});