angular.module('starter.services')

.factory('AthleteWorkoutService', function($http, Config, EndPoint, EstiloService, IntensidadeService, UserFriendlyService){

	var treinoAux         = {};
	var weekReferenceDate = undefined;
	
	var publicInterface = {

		setWeekReferenceDate : function(referenceDate){
			console.log("setWeekReferenceDate: " + referenceDate);
			weekReferenceDate = referenceDate;
		},

		getWeekReferenceDate : function(){
			console.log("getWeekReferenceDate: " + weekReferenceDate);
			return weekReferenceDate;
		},

		/**
			Buscar todos os treinos do técnico deste Atleta

			fromDate e toDate devem estar no formato: YYYY-MM-DD
		*/
		getMeusTreinos : function(fromDate, toDate, erroCallback, sucessoCallback){

			var idAtleta       = Config.getAtleta().id;
			var treinosPromise = $http.get( EndPoint.getTreinosDoAtleta(idAtleta, fromDate, toDate) );

			treinosPromise.then(function(resultado){

				console.log("resultado - lista de treinos", resultado);

				if(resultado.data.status){

					console.log("resultado.data.treinos", resultado.data.treinos);
					sucessoCallback(resultado.data.treinos);

				}else{
					/*
						Erro ao buscar os treinos do técnico deste atleta
					*/
					erroCallback("Erro ao buscar os treinos");
				}

			}, function(err){
				console.log("Erro ao buscar lista de intensidades", err);
				erroCallback(err);
			});

		},

		getWorkoutById : function(workoutId){

			var athlete = Config.getAtleta();

			//Verificar se os treinos do técnico deste atleta já estão em memória
			if(athlete.coachWorkouts && athlete.coachWorkouts.length > 0){
				return athlete
					.coachWorkouts
					.filter(function(w){
						return workoutId == w._id;
					})
					.pop();
			}else{
				console.log("Erro", "Treinos do técnico deste atleta não estão em memória ainda");
			}	
		},

		setTreinoAux : function(treino){
			console.log("setTreinoAux");
			treinoAux = treino;
		},

		getTreinoAux : function(){
			console.log("getTreinoAux");
			return treinoAux;
		},

		formatarTreinoParaHtml : function(treino){

			var formatarData = function(dateObj){
		      var dia = dateObj.getDate() < 10 ? "0" + dateObj.getDate() : dateObj.getDate();
		      var mes = dateObj.getMonth() + 1;
		      mes = mes < 10 ? "0" + mes : mes;
		      return "" + dia + "/" + mes + "/" + dateObj.getFullYear();
		  	};

		  	var formataBlocoSimples = function(bloco){
		  		var text = "" + bloco.series + " x " + bloco.repeticoes + " x " + bloco.distancia + "m " + EstiloService.getNomeEstilo(bloco.idEstilo) + " " + IntensidadeService.getNomeIntensidade(bloco.idIntensidade);
				text += " | Min: " + UserFriendlyService.fromSecondsToUserFriendlyTime(bloco.minTime) + " | Máx: " + UserFriendlyService.fromSecondsToUserFriendlyTime(bloco.maxTime) + " | A cada: " + bloco.aCada + "\n";
				return text;
		  	};

			var bodyText = "------------------------------------------------------------------\n";
			bodyText += "Treino: " + treino.treino.nome + "\n";
			bodyText += "Data: " + formatarData(new Date(treino.treino.data)) + "\n"
			bodyText += "------------------------------------------------------------------";
			bodyText += "\n\n";

			treino.blocos.forEach(function(bloco){

				if(bloco.isBlocoSimples){
					bodyText += formataBlocoSimples(bloco) + "\n";

				}else{

					bodyText += "" + bloco.repeticoes + " vezes | Intervalo de " + bloco.intervalo + " seg\n";

					bloco.blocos.forEach(function(blocoSimples){
						bodyText += formataBlocoSimples(blocoSimples);
					});

					bodyText += "\n";
				}
				
			});

			bodyText += "\n------------------------------------------------------------------\n";
			bodyText += "TOTAL " + treino.treino.distanciaTotal + " metros";
			bodyText += "\n------------------------------------------------------------------\n\n";

			return bodyText;
		}	
	};

	return publicInterface;
});