angular.module('starter.services')

.factory('AthleteService', function($http, Config, EndPoint){
	

	var publicInterface = {

		enviarSolicitacaoParaTecnico : function(coachEmail){

			return $http.get(

				EndPoint.atletaSolicitaTecnico(
					Config.getAtleta().id,
					coachEmail
				)
			);
		},

		/*
			successCallback - recebe como parâmetro uma lista (array) dos melhores tempos do atleta
		*/
		getMelhoresTempos : function(errorCallback, successCallback){
			var idAtleta = Config.getAtleta().id;
			console.log("getMelhoresTempos - idAtleta:", idAtleta);
			var promise = $http.get(EndPoint.getMelhoresTempos(idAtleta));

			promise.then(function(response){

				if(response.data.status){
					successCallback(response.data.melhoresTempos);

				}else{
					errorCallback();
				}

			}, function(err){
				console.log("Erro ao buscar lista de melhores tempos do atleta");
				errorCallback();
			});
		},

		updateMelhorTempo : function(idEstilo, tempo){
			var idAtleta = Config.getAtleta().id;

			var data = {
				idAtleta : idAtleta,
				idEstilo : idEstilo,
				tempo : tempo
			};

			return $http.post(
				EndPoint.updateMelhorTempo(),
				data
			);
		},

		/*
			callback deve ter dois parâmetros.
				primeiro -> true ou false
				segundo -> mensagem
		*/
		updateAtleta : function(atleta, callback){

			$http.post(
				EndPoint.updateAtleta(),
				{atleta: atleta}
			).then(function(response){

				if(response.data.status){
					Config.setAtleta(atleta);
					callback(true, response.data.msg);

				}else{
					console.log("Erro ao atualizar o atleta", err);
					callback(false, response.data.msg);	
				}

			}, function(err){
				console.log("Erro ao atualizar o atleta", err);
				callback(false, "Erro ao atualizar o atleta");
			});
		}


	};

	return publicInterface;
});