angular.module('starter.services')

.factory('NomesEventosService', function(){
	
	return {
		novaSolicitacao : "nova solicitacao",
		atletaAtualizado : "atleta atualizado",
		tecnicoAtualizado : "tecnico atualizado"
	};

});