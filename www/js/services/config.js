angular.module('starter.services')

.factory('Config', function($resource) {

  //var serverUrl = "http://192.168.0.12:8080";
  var serverUrl = "http://nodejs-ava1993.rhcloud.com";

  var sessao = {
      usuario : {},
      atleta : {},
      tecnico : {}
  };
  
  return {

  		getServerUrl : function(){
  			return serverUrl;
  		},

  		setUsuario : function(usuario){
        sessao.usuario = usuario;
  		},

      isAthlete : function(){
          if(sessao.usuario.isTecnico){
              return false;
          }else{
              return true;
          }
      },

  		getUsuario : function(){
  			return sessao.usuario;
  		},

      getTecnico : function(){
        return sessao.tecnico;
      },

      setTecnico : function(tecnico){
          sessao.tecnico = tecnico;
      },

      getAtleta : function(){
        return sessao.atleta;
      },

      setAtleta : function(atleta){
        sessao.atleta = atleta;
      }
  };

});