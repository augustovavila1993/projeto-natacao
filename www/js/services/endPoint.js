angular.module('starter.services')

/*
	Serviço que mapeia todos os end points da aplicação.

	Se o end point (a URL que se quer fazer um GET ou POST, por exemplo) precisa de algum parâmetro
	para ser criado, deve-se criar um função neste serviço que receberá o parâmetro e retornará o
	end point (URL) gerada.
*/
.factory('EndPoint', function(Config){

	var baseApi = Config.getServerUrl() + "/api";

	var publicInterface = {
		

		// ------------------------------- Requisições GET -------------------------------

		getListaIntensidadeZonaTreinamentoFraco : function(){
			return baseApi + "/treino/intervaloZonaTreinamentoFraco";
		},

		getListaIntensidadeZonaTreinamentoForte : function(){
			return baseApi + "/treino/intervaloZonaTreinamentoForte";
		},

		getListaIntervaloPorIntensidade : function(){
			return baseApi + "/treino/intervaloPorIntensidade";
		},

		getTreinosDoAtleta : function(idAtleta, fromDate, toDate){
			return baseApi + "/atleta/" + idAtleta + "/meus-treinos/from/" + fromDate + "/to/" + toDate;	
		},

		getTreinosDoTecnico : function(idTecnico, fromDate, toDate){
			return baseApi + "/tecnico/" + idTecnico + "/meus-treinos/from/" + fromDate + "/to/" + toDate;	
		},

		getListaDeIntensidades : function(){
			return baseApi + "/treino/intensidadesList";	
		},

		getListaDeEstilos : function(){
			return baseApi + "/treino/estilosList";	
		},

		atletaSolicitaTecnico : function(idAtleta, emailTecnico){
			return baseApi + "/atleta/" + idAtleta + "/solicitar-tecnico/" + emailTecnico;
		},

		aceitarSolicitacaoDoAtleta : function(idAtleta){
			return baseApi + "/tecnico/aceitar-solicitacao-atleta/" + idAtleta;
		},

		recusarSolicitacaoDoAtleta : function(idAtleta){
			return baseApi + "/tecnico/recusar-solicitacao-atleta/" + idAtleta;
		},

		getAtletasPendentesDoTecnico : function(idTecnico){
			return baseApi + "/tecnico/" + idTecnico + "/buscar-atletas-pendentes";
		},	

		getAtletasConfirmadosDoTecnico : function(idTecnico){
			return baseApi + "/tecnico/" + idTecnico + "/buscar-atletas-confirmados";
		},

		getMelhoresTempos : function(idAtleta){
			return baseApi + "/atleta/" + idAtleta + "/melhores_tempos";	
		},

		excluirTreino : function(idTreino){
			return baseApi + "/treino/" + idTreino + "/excluir";
		},


		// ------------------------------- Requisições POST -------------------------------

		novoUsuario : function(){
			return baseApi + "/usuario";
		},

		autenticarUsuario : function(){
			return baseApi + "/usuario/autenticar";
		},

		updateMelhorTempo : function(){
			return baseApi + "/atleta/updateMelhorTempo";	
		},

		updateAtleta : function(){
			return baseApi + "/atleta/updateAtleta";		
		},

		updateUsuario : function(){
			return baseApi + "/usuario/updateUsuario";
		}


		// ------------------------------- Requisições PUT -------------------------------

		
	};

	return publicInterface;
});