angular.module('starter.services')

.factory('CoachWorkoutService', function(Config, $http, EndPoint, EstiloService, IntensidadeService){
	
	var treinoAux         = undefined;
	var weekReferenceDate = undefined;

	var publicInterface = {

		setWeekReferenceDate : function(referenceDate){
			console.log("setWeekReferenceDate: " + referenceDate);
			weekReferenceDate = referenceDate;
		},

		getWeekReferenceDate : function(){
			console.log("getWeekReferenceDate: " + weekReferenceDate);
			return weekReferenceDate;
		},

		/**
			Buscar todos os treinos do técnico

			fromDate e toDate devem estar no formato: YYYY-MM-DD
		*/
		getMeusTreinos : function(fromDate, toDate, erroCallback, sucessoCallback){

			var idTecnico       = Config.getTecnico().id;
			var treinosPromise = $http.get( EndPoint.getTreinosDoTecnico(idTecnico, fromDate, toDate) );

			treinosPromise.then(function(resultado){

				console.log("resultado - lista de treinos", resultado);

				if(resultado.data.status){

					console.log("resultado.data.treinos", resultado.data.treinos);
					sucessoCallback(resultado.data.treinos);

				}else{
					/*
						Erro ao buscar os treinos do técnico deste atleta
					*/
					erroCallback("Erro ao buscar os treinos");
				}

			}, function(err){
				console.log("Erro ao buscar lista de intensidades", err);
				erroCallback(err);
			});
		},

		/*
			Retorna uma promise (.then(successCallbackFunc, erroCallbackFunc))
		*/
		postTreino : function(treino){

			var tecnico = Config.getTecnico();
			console.log("tecnico", tecnico);
			var createWorkoutUrl = Config.getServerUrl() + "/api/treino/" + tecnico.id + "/novo_treino";
			console.log("createWorkoutUrl", createWorkoutUrl);
			return $http.post( createWorkoutUrl, treino );
		},

		getWorkoutById : function(workoutId){
			return publicInterface
				.getWorkouts()
				.filter(function(w){
					return w._id == workoutId;
				})
				.pop();
		},

		setTreinoAux : function(treino){
			treinoAux = treino;
		},

		getTreinoAux : function(){
			return treinoAux;
		},

		excluirTreino : function(idTreino, erroCallback, sucessoCallback){
			var promise = $http.get( EndPoint.excluirTreino(idTreino) );

			promise.then(function(resultado){

				console.log("resultado da exclusão do treino", resultado);

				if(resultado.data.status){
					sucessoCallback();

				}else{
					erroCallback("Erro ao excluir o treino");
				}

			}, function(err){
				erroCallback("Erro ao excluir o treino");
			});
		},

		formatarTreinoParaHtml : function(treino){

			var formatarData = function(dateObj){
		      var dia = dateObj.getDate() < 10 ? "0" + dateObj.getDate() : dateObj.getDate();
		      var mes = dateObj.getMonth() + 1;
		      mes = mes < 10 ? "0" + mes : mes;
		      return "" + dia + "/" + mes + "/" + dateObj.getFullYear();
		  	};

		  	var formataBlocoSimples = function(bloco){
		  		return "" + bloco.series + " x " + bloco.repeticoes + " x " + bloco.distancia + "m " + EstiloService.getNomeEstilo(bloco.idEstilo) + " " + IntensidadeService.getNomeIntensidade(bloco.idIntensidade) + "\n";
		  	};

			var bodyText = "------------------------------------------------------------------\n";
			bodyText += "Treino: " + treino.treino.nome + "\n";
			bodyText += "Data: " + formatarData(new Date(treino.treino.data)) + "\n"
			bodyText += "------------------------------------------------------------------";
			bodyText += "\n\n";

			treino.blocos.forEach(function(bloco){

				if(bloco.isBlocoSimples){
					bodyText += formataBlocoSimples(bloco) + "\n";

				}else{

					bodyText += "" + bloco.repeticoes + " vezes | Intervalo de " + bloco.intervalo + " seg\n";

					bloco.blocos.forEach(function(blocoSimples){
						bodyText += formataBlocoSimples(blocoSimples);
					});

					bodyText += "\n";
				}
				
			});

			bodyText += "\n------------------------------------------------------------------\n";
			bodyText += "TOTAL " + treino.treino.distanciaTotal + " metros";
			bodyText += "\n------------------------------------------------------------------\n\n";

			return bodyText;
		}

	};

	return publicInterface;
});