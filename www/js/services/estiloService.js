angular.module('starter.services')

.factory('EstiloService', function($http, Config, EndPoint){

	/*
		É um array que guarda objetos de intensidade que seguem o formato:
		{
			id : "0", // id do estilo que foi cadastrado no backend
			nome : "nome teste" // nome do estilo cadastrado no backend
		}
	*/
	var estilos = [];

	var publicInterface = {

		getEstilos : function(){
			return estilos;
		},

		setEstilos : function(estilosArray){
			console.log("setEstilos estilosArray", estilosArray);
			estilos.length = 0;

			if(estilosArray && estilosArray.length > 0){
				estilosArray.forEach(function(estiloInput){
					estilos.push(estiloInput);
				});
			}

			console.log("Depois do Set", estilos);
		},

		getNomeEstilo : function (id){

			if(estilos.length > 0){

				return estilos.filter(function(estilo){
					return estilo.id == id;
				}).pop().nome;

			}else{
				return "Estilo não encontrado";
			}
		},

		getIdEstilo : function(nomeEstilo){
			if(estilos.length > 0){

				return estilos.filter(function(estilo){
					return estilo.nome.toLowerCase() == nomeEstilo.toLowerCase();
				}).pop().id;

			}else{
				return "Estilo não encontrado";
			}
		},

		/*
			Fará uma requisição GET para buscar a lista de estilos 
				cadastrados no backend

			Depois de buscar a lista de estilos, inseri-los no atributo deste Service
		*/
		getAndRefreshEstilosListFromServer : function(errorCallback, successCallback){
			var estilosPromise = $http.get( EndPoint.getListaDeEstilos() );

			estilosPromise.then(function(result){
				console.log("result - lista de estilos", result);
				console.log("estilos", result.data.estilos);
				publicInterface.setEstilos(result.data.estilos);
				successCallback();

			}, function(err){
				console.log("Erro ao buscar lista de estilos", err);
				errorCallback(err);
			});
		}
	};

	return publicInterface;
});