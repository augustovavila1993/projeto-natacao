angular.module('starter.services')

.factory('MyAthletesService', function($http, Config, EndPoint){
	

	var publicInterface = {

		/**
			Retorna um objeto Promise
		*/
		aceitarSolicitacaoDoAtleta : function(idAtleta){

			console.log("idAtleta", idAtleta);

			return $http.get(
				EndPoint.aceitarSolicitacaoDoAtleta(idAtleta)
			);
		},

		/**
			Retorna um objeto Promise
		*/
		recusarSolicitacaoDoAtleta : function(idAtleta){

			console.log("idAtleta", idAtleta);

			return $http.get(
				EndPoint.recusarSolicitacaoDoAtleta(idAtleta)
			);
		},


		/** 
			Buscar listas de atletas que enviaram solicitação para este técnico mas que ainda estão pendentes
		*/
		getAtletasPendentesDoTecnico : function(idTecnico){
			var idTecnico = Config.getTecnico().id;
			return $http.get( EndPoint.getAtletasPendentesDoTecnico(idTecnico) );
		},

		/** 
			Buscar listas de atletas confirmados do técnico
		*/
		getAtletasConfirmadosDoTecnico : function(idTecnico){
			var idTecnico = Config.getTecnico().id;
			return $http.get( EndPoint.getAtletasConfirmadosDoTecnico(idTecnico) );
		}

	};

	return publicInterface;
});