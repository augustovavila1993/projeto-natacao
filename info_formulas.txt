Fórmula para calcular a estimativa do tempo para uma determinada metragem

Tempo Estimado = (tempo real) * ( (distância estimada) / (distância real) )^1.09



--------------------------------------------------------------------------------------

IF(AND(M7="A0",P7<=100),U7*Configurações!$E$5


-----------------------------------
Intervalo Tiros

Menor
IF(AND(M7="A2",P7<=100),V7+Configurações!$N$7,

tempo_min_a_ser_feito + 

=IF(AND(M7="A0",P7<=100),V7+Configurações!$N$5,

IF(AND(M7="A0",P7>100,P7<400),V7+Configurações!$O$5,

IF(AND(M7="A0",P7>=400),V7+Configurações!$P$5,

IF(AND(M7="A1",P7<=100),V7+Configurações!$N$6,

IF(AND(M7="A1",P7>100,P7<400),V7+Configurações!$O$6,

IF(AND(M7="A1",P7>=400),V7+Configurações!$P$6,

IF(AND(M7="A2",P7<=100),V7+Configurações!$N$7,

IF(AND(M7="A2",P7>100,P7<400),V7+Configurações!$O$7,

IF(AND(M7="A2",P7>=400),V7+Configurações!$P$7,

IF(AND(M7="A3",P7<=100),V7+Configurações!$N$8,

IF(AND(M7="A3",P7>100,P7<400),V7+Configurações!$O$8,

IF(AND(M7="A3",P7>=400),V7+Configurações!$P$8,

IF(AND(M7="RESAN",P7<=100),V7+Configurações!$N$9,

IF(AND(M7="RESAN",P7>100,P7<400),V7+Configurações!$O$9,

IF(AND(M7="RESAN",P7>=400),V7+Configurações!$P$9,

IF(AND(M7="TL",P7<=100),V7+Configurações!$N$10,

IF(AND(M7="TL",P7>100,P7<400),V7+Configurações!$O$10,

IF(AND(M7="TL",P7>=400),V7+Configurações!$P$10,

IF(AND(M7="PT",P7<=100),V7+Configurações!$N$11,

IF(AND(M7="PT",P7>100,P7<400),V7+Configurações!$O$11,

IF(AND(M7="PT",P7>=400),V7+Configurações!$P$11,

IF(AND(M7="VL",P7<=100),V7+Configurações!$N$12))))))))))))))))))))))


0:00:30	0:00:45	0:01:00	A0
0:00:10	0:00:20	0:00:30	A1
0:00:20	0:00:30	0:00:45	A2
0:01:00	0:01:15	0:01:30	A3
0:02:00	0:02:30	0:03:00	RESAN
0:03:00	0:04:00	0:05:00	TL
0:06:00	0:08:00	0:10:00	PT
0:00:45	x	x	VL
